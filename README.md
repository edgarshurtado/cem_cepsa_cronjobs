# CEPSA CRONJOBS

Este es un projecto derivado del principal de CEPSA que se encargará de
realizar las tareas automáticas que se requieran.

## Rutas disponibles

* Carga completa de todo lo que hay en web/uploads `web/import/cargar-todo`
* Carga del fichero con fecha de ayer `web/import/autocarga`
* Descargar todos los ficheros presentes en el sftp `web/import/descargar-todo`
* Rutas asociadas a comandos:
    - `web/rutina/borrar-encuestas`
    - `web/rutina/poblar-encuesta-procesos`
    - `web/rutina/poblar-permutaciones-vistas`
    - `web/rutina/poblar-nosql-encuestas`
    - `web/rutina/poblar-nosql-encuesta-procesos`
    - `web/rutina/poblar-nosql-encuestas-datatable`

## Comandos disponibles
* `madison:borrar-encuestas`
* `madison:poblar-permutaciones-vistas`
* `madison:poblar-encuesta-procesos`
* `madison:poblar-nosql-encuestas`
* `madison:poblar-nosql-encuesta-procesos`
* `madison:poblar-nosql-encuestas-datatable`

# SQL de borrado de las encuestas en la aplicación

```MySql
BEGIN;
DELETE FROM `nosql_encuestas_datatable`where 1;
DELETE FROM `survey_encuesta_respuesta` where 1;
DELETE FROM `survey_encuesta_proceso` where 1;
DELETE FROM `survey_encuesta` where 1;
DELETE FROM `admin_permutaciones_vistas` where 1;
DELETE FROM `negocio_producto` where 1;
DELETE FROM `negocio_linea_negocio` where 1;

DELETE FROM `admin_cliente` where 1;
ALTER TABLE `admin_cliente` AUTO_INCREMENT = 1;
INSERT INTO `admin_cliente` (id) VALUES (1);
ALTER TABLE `survey_encuesta` AUTO_INCREMENT = 1;
ALTER TABLE `survey_encuesta_respuesta` AUTO_INCREMENT = 1;
ALTER TABLE `survey_encuesta_proceso` AUTO_INCREMENT = 1;
ALTER TABLE `admin_permutaciones_vistas` AUTO_INCREMENT = 1;
ALTER TABLE `negocio_producto` AUTO_INCREMENT = 1;
ALTER TABLE `negocio_linea_negocio` AUTO_INCREMENT = 1;
ALTER TABLE `nosql_encuestas_datatable`AUTO_INCREMENT = 1;

DELETE FROM `admin_canal_det_canal` where 1;
ALTER TABLE `admin_canal_det_canal` AUTO_INCREMENT = 1;

DELETE FROM `admin_producto` WHERE id > 4;
ALTER TABLE `admin_producto` AUTO_INCREMENT = 5;
DELETE FROM `admin_negocio` WHERE id > 15;
ALTER TABLE `admin_negocio` AUTO_INCREMENT = 16;
COMMIT;
```

# SQL para asignación de lineas de negocio según la opi_pregunta

```sql
UPDATE `survey_encuesta` SET `org_id` = 2
            WHERE opi_pregunta_id = 7
    OR opi_pregunta_id = 9
    OR opi_pregunta_id = 10
    OR opi_pregunta_id = 31
    OR opi_pregunta_id = 11
    OR opi_pregunta_id = 12
    OR opi_pregunta_id = 13
    OR opi_pregunta_id = 8
    OR opi_pregunta_id = 14;

UPDATE `survey_encuesta` SET `org_id` = 3
            WHERE opi_pregunta_id = 15
    OR opi_pregunta_id = 4;

UPDATE `survey_encuesta` SET `org_id` = 4
            WHERE opi_pregunta_id = 32;

UPDATE `survey_encuesta` SET `org_id` = 5
            WHERE opi_pregunta_id = 2;

UPDATE `survey_encuesta` SET `org_id` = 6
            WHERE opi_pregunta_id = 18
    OR opi_pregunta_id = 33;

UPDATE `survey_encuesta` SET `org_id` = 7
            WHERE opi_pregunta_id = 19;

UPDATE `survey_encuesta` SET `org_id` = 8
            WHERE opi_pregunta_id = 20
    OR opi_pregunta_id = 21;

UPDATE `survey_encuesta` SET `org_id` = 9
            WHERE opi_pregunta_id = 22
    OR opi_pregunta_id = 23
    OR opi_pregunta_id = 24
    OR opi_pregunta_id = 25
    OR opi_pregunta_id = 26
    OR opi_pregunta_id = 27
    OR opi_pregunta_id = 28
    OR opi_pregunta_id = 29
    OR opi_pregunta_id = 30
    OR opi_pregunta_id = 34
    OR opi_pregunta_id = 35
    OR opi_pregunta_id = 36;
```

# SQL para generación tabla desnormalizada para Encuestas datatable
```
INSERT INTO nosql_encuestas_datatable (encuesta_id, fecha, negocio, linea_negocio,
    canal, detalle_canal, procesos, segmento, servicio, experiencia, recomendacion,
    sentimiento, opi, warning)
SELECT
                e.id as encuesta_id,
                e.fecha as fecha,
                neg.nombre as negocio,
                org.nombre as linea_negocio,
                can.nombre as canal,
                det_can.nombre as detalle_canal,
                 (
                    SELECT GROUP_CONCAT(p.nombre SEPARATOR ', ')
                        FROM survey_encuesta_proceso eproceso
                            JOIN admin_proceso p ON eproceso.proceso_id = p.id
                        WHERE eproceso.encuesta_id = e.id
                ) as procesos,
                seg.nombre as segmento,
                ser.nombre as servicio,
                SUM(IF(er.encuesta_pregunta_id=3,er.valor,0)) as 'experiencia',
                SUM(IF(er.encuesta_pregunta_id=2,er.valor,0)) as 'recomendacion',
                SUM(IF(er.encuesta_pregunta_id=1, er.ev, 0)) as 'sentimiento',
                opi.nombre as opi,
                (SELECT IF(SUM(IF(er_warning.valor < 5, 1,0)) = 0, 0, 1) as warning
                    FROM `survey_encuesta_respuesta` er_warning
                    JOIN `survey_encuesta_pregunta` ep_warning ON er_warning.encuesta_pregunta_id = ep_warning.id
                    WHERE ep_warning.cem_tipo_id = 4 && er_warning.encuesta_id = er.encuesta_id
                ) as 'warning'
                FROM survey_encuesta_respuesta er
                    JOIN survey_encuesta e ON e.id = er.encuesta_id
                    JOIN survey_encuesta_pregunta_opi opi ON opi.id = e.opi_pregunta_id
                    join admin_negocio neg ON e.neg_id = neg.id
                    JOIN admin_canal_encuestacion can ON e.can_id = can.id
                    JOIN admin_linea_negocio org ON e.org_id = org.id
                    JOIN admin_determinacion_canal det_can ON e.det_can = det_can.id
                    JOIN admin_segmento seg ON e.seg_id = seg.id
                    JOIN admin_servicio ser ON e.ser1_id = ser.id
                GROUP BY e.id
```


# SQL para poblar nosql_encuestas

```
INSERT INTO nosql_encuestas (
    encuesta_id, fecha, opi_pregunta_id, opi_pregunta_string, opi_id,
    segmento_id, segmento_string, negocio_id, negocio_string,
    linea_negocio_id, linea_negocio_string, canal_id, canal_string,
    detalle_canal_id, detalle_canal_string, servicio_id, servicio_string,
    g1, g1_valor, g2, g3, a1, gc1, gc2, gc3, gc4, of1, of2, of3, of4,
    p2, pc1, pc2, ps1, ps2
)

SELECT
            e.id,
            e.fecha as fecha,
            e.opi_pregunta_id as opi_pregunta_id,
            opi_pregunta.nombre as opi_pregunta_string,
            e.opi_id as opi_id,
            e.seg_id as segmento_id,
            admin_segmento.nombre as segmento_string,
            e.neg_id as negocio_id,
            admin_negocio.nombre as negocio_string,
            e.org_id as linea_negocio_id,
            admin_linea_negocio.nombre as linea_negocio_string,
            e.can_id as canal_id,
            admin_canal_encuestacion.nombre as canal_string,
            e.det_can as detalle_canal_id,
            admin_determinacion_canal.nombre as detalle_canal_string,
            e.ser1_id as servicio_id,
            admin_servicio.nombre as servicio_string,
            (
                SELECT g1.valor
                    FROM survey_encuesta_respuesta g1
                    WHERE g1.encuesta_pregunta_id = 1
                    AND g1.encuesta_id = e.id

            ) as 'g1',
            (
                SELECT g1.ev
                    FROM survey_encuesta_respuesta g1
                    WHERE g1.encuesta_pregunta_id = 1
                    AND g1.encuesta_id = e.id

            ) as 'g1_valor',
            (
                SELECT g2.valor
                    FROM survey_encuesta_respuesta g2
                    WHERE g2.encuesta_pregunta_id = 2
                    AND g2.encuesta_id = e.id

            ) as 'g2',
            (
                SELECT g3.valor
                    FROM survey_encuesta_respuesta g3
                    WHERE g3.encuesta_pregunta_id = 3
                    AND g3.encuesta_id = e.id

            ) as 'g3',
            (
                SELECT a1.valor
                    FROM survey_encuesta_respuesta a1
                    WHERE a1.encuesta_pregunta_id = 15
                    AND a1.encuesta_id = e.id

            ) as 'a1',
            (
                SELECT gc1.valor
                    FROM survey_encuesta_respuesta gc1
                    WHERE gc1.encuesta_pregunta_id = 23
                    AND gc1.encuesta_id = e.id

            ) as 'gc1',
            (
                SELECT gc2.valor
                    FROM survey_encuesta_respuesta gc2
                    WHERE gc2.encuesta_pregunta_id = 25
                    AND gc2.encuesta_id = e.id

            ) as 'gc2',
            (
                SELECT gc3.valor
                    FROM survey_encuesta_respuesta gc3
                    WHERE gc3.encuesta_pregunta_id = 26
                    AND gc3.encuesta_id = e.id

            ) as 'gc3',
            (
                SELECT gc4.valor
                    FROM survey_encuesta_respuesta gc4
                    WHERE gc4.encuesta_pregunta_id = 8
                    AND gc4.encuesta_id = e.id

            ) as 'gc4',
            (
                SELECT of1.valor
                    FROM survey_encuesta_respuesta of1
                    WHERE of1.encuesta_pregunta_id = 5
                    AND of1.encuesta_id = e.id

            ) as 'of1',
            (
                SELECT of2.valor
                    FROM survey_encuesta_respuesta of2
                    WHERE of2.encuesta_pregunta_id = 6
                    AND of2.encuesta_id = e.id

            ) as 'of2',
            (
                SELECT of3.valor
                    FROM survey_encuesta_respuesta of3
                    WHERE of3.encuesta_pregunta_id = 4
                    AND of3.encuesta_id = e.id

            ) as 'of3',
            (
                SELECT of4.valor
                    FROM survey_encuesta_respuesta of4
                    WHERE of4.encuesta_pregunta_id = 22
                    AND of4.encuesta_id = e.id

            ) as 'of4',
            (
                SELECT p2.valor
                    FROM survey_encuesta_respuesta p2
                    WHERE p2.encuesta_pregunta_id = 7
                    AND p2.encuesta_id = e.id

            ) as 'p2',
            (
                SELECT pc1.valor
                    FROM survey_encuesta_respuesta pc1
                    WHERE pc1.encuesta_pregunta_id = 20
                    AND pc1.encuesta_id = e.id

            ) as 'pc1',
            (
                SELECT pc2.valor
                    FROM survey_encuesta_respuesta pc2
                    WHERE pc2.encuesta_pregunta_id = 24
                    AND pc2.encuesta_id = e.id

            ) as 'pc2',
            (
                SELECT ps1.valor
                    FROM survey_encuesta_respuesta ps1
                    WHERE ps1.encuesta_pregunta_id = 9
                    AND ps1.encuesta_id = e.id

            ) as 'ps1',
            (
                SELECT ps2.valor
                    FROM survey_encuesta_respuesta ps2
                    WHERE ps2.encuesta_pregunta_id = 21
                    AND ps2.encuesta_id = e.id

            ) as 'ps2'
           FROM survey_encuesta e
            JOIN survey_encuesta_pregunta_opi opi_pregunta ON opi_pregunta.id = e.opi_pregunta_id
            JOIN admin_segmento ON admin_segmento.id = e.seg_id
            JOIN admin_negocio ON admin_negocio.id = e.neg_id
            JOIN admin_linea_negocio ON admin_linea_negocio.id = e.org_id
            JOIN admin_canal_encuestacion ON admin_canal_encuestacion.id = e.can_id
            JOIN admin_determinacion_canal ON admin_determinacion_canal.id = e.det_can
            JOIN admin_servicio ON admin_servicio.id = e.ser1_id
           WHERE e.id NOT IN (
            SELECT id FROM nosql_encuestas
           )
```
13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
