<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Encuesta
 *
 * @ORM\Table(name="survey_encuesta")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaRepository")
 */
class Encuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="SurveyBundle\Entity\EncuestaPreguntaOpi")
     */
    private $opiPregunta;


    /**
     * @ORM\OneToMany(targetEntity="EncuestaRespuesta", mappedBy="encuesta")
     */
    private $encuestaRespuestas;

    /**
     * @ORM\Column(name="opi_id", type="string", length=255)
     */
    private $opiId;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Cliente", inversedBy="encuestas", cascade={"persist"})
     */
    private $cliente;


    // VARIABLES DE FILTRADO
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Segmento", inversedBy="encuestas", cascade={"persist"})
     */
    private $seg;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Negocio", inversedBy="encuestas")
     */
    private $neg;

    /**
     * @ORM\Column(name="nconcn", type="string", length=256)
     */
    private $nconcn;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\LineaNegocio", inversedBy="encuestas")
     */
    private $org;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Zona", inversedBy="encuestas", cascade={"persist"})
     */
    private $zon;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Localidad", inversedBy="encuestas", cascade={"persist"})
     */
    private $loc;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Provincia", inversedBy="encuestas", cascade={"persist"})
     */
    private $prov;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Pais", inversedBy="encuestas", cascade={"persist"})
     */
    private $pais;

    /**
     * @ORM\Column(name="lat", type="string", length=255)
     */
    private $lat;

    /**
     * @ORM\Column(name="lon", type="string", length=255)
     */
    private $lon;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\CanalEncuestacion", inversedBy="encuestas")
     */
    private $can;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\DeterminacionCanal",
     inversedBy="encuestas")
     * @ORM\JoinColumn(name="det_can", referencedColumnName="id")
     */
    private $detCan;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TipificacionOpinion", inversedBy="encuestas", cascade={"persist"})
     */
    private $tip1;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TipificacionOpinion", inversedBy="encuestas2", cascade={"persist"})
     */
    private $tip2;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TipificacionOpinion", inversedBy="encuestas3", cascade={"persist"})
     */
    private $tip3;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TipificacionOpinion", inversedBy="encuestas4", cascade={"persist"})
     */
    private $tip4;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio", inversedBy="encuestas", cascade={"persist"})
     */
    private $ser1;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio", inversedBy="encuestas2", cascade={"persist"})
     */
    private $ser2;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio", inversedBy="encuestas3", cascade={"persist"})
     */
    private $ser3;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Servicio", inversedBy="encuestas4", cascade={"persist"})
     */
    private $ser4;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestaRespuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuestaRespuestas
     *
     * @param \SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas
     * @return Encuesta
     */
    public function addEncuestaRespuesta(\SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas)
    {
        $this->encuestaRespuestas[] = $encuestaRespuestas;

        return $this;
    }

    /**
     * Remove encuestaRespuestas
     *
     * @param \SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas
     */
    public function removeEncuestaRespuesta(\SurveyBundle\Entity\EncuestaRespuesta $encuestaRespuestas)
    {
        $this->encuestaRespuestas->removeElement($encuestaRespuestas);
    }

    /**
     * Get encuestaRespuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestaRespuestas()
    {
        return $this->encuestaRespuestas;
    }

    /**
     * Set nconcn
     *
     * @param string $nconcn
     * @return Encuesta
     */
    public function setNconcn($nconcn)
    {
        $this->nconcn = $nconcn;

        return $this;
    }

    /**
     * Get nconcn
     *
     * @return string
     */
    public function getNconcn()
    {
        return $this->nconcn;
    }

    /**
     * Set org
     *
     * @param string $org
     * @return Encuesta
     */
    public function setOrg($org)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return string
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * Set zon
     *
     * @param string $zon
     * @return Encuesta
     */
    public function setZon($zon)
    {
        $this->zon = $zon;

        return $this;
    }

    /**
     * Get zon
     *
     * @return string
     */
    public function getZon()
    {
        return $this->zon;
    }

    /**
     * Set loc
     *
     * @param string $loc
     * @return Encuesta
     */
    public function setLoc($loc)
    {
        $this->loc = $loc;

        return $this;
    }

    /**
     * Get loc
     *
     * @return string
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set prov
     *
     * @param string $prov
     * @return Encuesta
     */
    public function setProv($prov)
    {
        $this->prov = $prov;

        return $this;
    }

    /**
     * Get prov
     *
     * @return string
     */
    public function getProv()
    {
        return $this->prov;
    }

    /**
     * Set pais
     *
     * @param string $pais
     * @return Encuesta
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Encuesta
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return Encuesta
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set can
     *
     * @param string $can
     * @return Encuesta
     */
    public function setCan($can)
    {
        $this->can = $can;

        return $this;
    }

    /**
     * Get can
     *
     * @return string
     */
    public function getCan()
    {
        return $this->can;
    }

    /**
     * Set detCan
     *
     * @param string $detCan
     * @return Encuesta
     */
    public function setDetCan($detCan)
    {
        $this->detCan = $detCan;

        return $this;
    }

    /**
     * Get detCan
     *
     * @return string
     */
    public function getDetCan()
    {
        return $this->detCan;
    }


    /**
     * Función para realizar una comprobación de que los campos set y tip
     * no tienen valores repetidos. Con esto evito que a la hora de persistir
     * el objeto, me guarde como set/tip distintos valores que realmente son
     * el mismo.
     */
    private function comprobacionSetTipYSer($campos, $campoSet)
    {
        foreach ($campos as $campo) {
            if( isset($this->$campo) &&
                $this->$campo->getNombre() == $campoSet->getNombre()){

                return $this->$campo;
            }
        }
        return $campoSet;
    }

    /**
     * Set tip1
     *
     * @param string $tip1
     * @return Encuesta
     */
    public function setTip1($tip1)
    {
        $this->tip1 =
            $this->comprobacionSetTipYSer(["tip2", "tip3", "tip4"], $tip1);

        return $this;
    }

    /**
     * Get tip1
     *
     * @return string
     */
    public function getTip1()
    {
        return $this->tip1;
    }

    /**
     * Set tip2
     *
     * @param string $tip2
     * @return Encuesta
     */
    public function setTip2($tip2)
    {
        $this->tip2 =
            $this->comprobacionSetTipYSer(["tip1", "tip3", "tip4"], $tip2);

        return $this;
    }

    /**
     * Get tip2
     *
     * @return string
     */
    public function getTip2()
    {
        return $this->tip2;
    }

    /**
     * Set tip3
     *
     * @param string $tip3
     * @return Encuesta
     */
    public function setTip3($tip3)
    {
        $this->tip3 =
            $this->comprobacionSetTipYSer(["tip1", "tip2", "tip4"], $tip3);

        return $this;
    }

    /**
     * Get tip3
     *
     * @return string
     */
    public function getTip3()
    {
        return $this->tip3;
    }

    /**
     * Set tip4
     *
     * @param string $tip4
     * @return Encuesta
     */
    public function setTip4($tip4)
    {
        $this->tip4 =
            $this->comprobacionSetTipYSer(["tip1", "tip2", "tip3"], $tip4);

        return $this;
    }

    /**
     * Get tip4
     *
     * @return string
     */
    public function getTip4()
    {
        return $this->tip4;
    }

    /**
     * Set ser1
     *
     * @param string $ser1
     * @return Encuesta
     */
    public function setSer1($ser1)
    {
        $this->ser1 =
            $this->comprobacionSetTipYSer(["ser2", "ser3", "ser1"], $ser1);

        return $this;
    }

    /**
     * Get ser1
     *
     * @return string
     */
    public function getSer1()
    {
        return $this->ser1;
    }

    /**
     * Set ser2
     *
     * @param string $ser2
     * @return Encuesta
     */
    public function setSer2($ser2)
    {
        $this->ser2 =
            $this->comprobacionSetTipYSer(["ser1", "ser3", "ser4"], $ser2);

        return $this;
    }

    /**
     * Get ser2
     *
     * @return string
     */
    public function getSer2()
    {
        return $this->ser2;
    }

    /**
     * Set ser3
     *
     * @param string $ser3
     * @return Encuesta
     */
    public function setSer3($ser3)
    {
        $this->ser3 =
            $this->comprobacionSetTipYSer(["ser1", "ser2", "ser4"], $ser3);

        return $this;
    }

    /**
     * Get ser3
     *
     * @return string
     */
    public function getSer3()
    {
        return $this->ser3;
    }

    /**
     * Set ser4
     *
     * @param string $ser4
     * @return Encuesta
     */
    public function setSer4($ser4)
    {
        $this->ser4 =
            $this->comprobacionSetTipYSer(["ser1", "ser2", "ser3"], $ser4);

        return $this;
    }

    /**
     * Get ser4
     *
     * @return string
     */
    public function getSer4()
    {
        return $this->ser4;
    }

    /**
     * Set cliente
     *
     * @param \AdminBundle\Entity\Cliente $cliente
     * @return Encuesta
     */
    public function setCliente(\AdminBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \AdminBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set seg
     *
     * @param string $seg
     * @return Encuesta
     */
    public function setSeg($seg)
    {
        $this->seg = $seg;

        return $this;
    }

    /**
     * Get seg
     *
     * @return string
     */
    public function getSeg()
    {
        return $this->seg;
    }

    /**
     * Set neg
     *
     * @param string $neg
     * @return Encuesta
     */
    public function setNeg($neg)
    {
        $this->neg = $neg;

        return $this;
    }

    /**
     * Get neg
     *
     * @return string
     */
    public function getNeg()
    {
        return $this->neg;
    }

    /**
     * Set opiId
     *
     * @param string $opiId
     * @return Encuesta
     */
    public function setOpiId($opiId)
    {
        $this->opiId = $opiId;

        return $this;
    }

    /**
     * Get opiId
     *
     * @return string
     */
    public function getOpiId()
    {
        return $this->opiId;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Encuesta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set opiPreguntaId
     *
     * @param string $opiPreguntaId
     * @return Encuesta
     */
    public function setOpiPreguntaId($neg)
    {
        $this->opiPreguntaId = $opiPreguntaId;

        return $this;
    }

    /**
     * Get opiPreguntaId
     *
     * @return string
     */
    public function getOpiPreguntaId()
    {
        return $this->opiPreguntaId;
    }

    /**
     * Set OPI
     *
     * @param string $oPI
     * @return Encuesta
     */
    public function setOPI($oPI)
    {
        $this->OPI = $oPI;

        return $this;
    }

    /**
     * Get OPI
     *
     * @return string
     */
    public function getOPI()
    {
        return $this->OPI;
    }

    /**
     * Set opiPregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta
     * @return Encuesta
     */
    public function setOpiPregunta(\SurveyBundle\Entity\EncuestaPreguntaOpi $opiPregunta = null)
    {
        $this->opiPregunta = $opiPregunta;

        return $this;
    }

    /**
     * Get opiPregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPreguntaOpi
     */
    public function getOpiPregunta()
    {
        return $this->opiPregunta;
    }
}
