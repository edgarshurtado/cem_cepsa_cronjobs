<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaSubBloque
 *
 * @ORM\Table(name="survey_encuesta_sub_bloque")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaSubBloqueRepository")
 */
class EncuestaSubBloque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaBloque", inversedBy="subBloques")
     */
    private $bloque;

    /**
     * @ORM\OneToMany(targetEntity="EncuestaPregunta", mappedBy="subBloque")
     */
    private $preguntas;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EncuestaSubBloque
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set bloque
     *
     * @param \SurveyBundle\Entity\EncuestaBloque $bloque
     * @return EncuestaSubBloque
     */
    public function setBloque(\SurveyBundle\Entity\EncuestaBloque $bloque = null)
    {
        $this->bloque = $bloque;

        return $this;
    }

    /**
     * Get bloque
     *
     * @return \SurveyBundle\Entity\EncuestaBloque
     */
    public function getBloque()
    {
        return $this->bloque;
    }

    /**
     * Add preguntas
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $preguntas
     * @return EncuestaSubBloque
     */
    public function addPregunta(\SurveyBundle\Entity\EncuestaPregunta $preguntas)
    {
        $this->preguntas[] = $preguntas;

        return $this;
    }

    /**
     * Remove preguntas
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $preguntas
     */
    public function removePregunta(\SurveyBundle\Entity\EncuestaPregunta $preguntas)
    {
        $this->preguntas->removeElement($preguntas);
    }

    /**
     * Get preguntas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPreguntas()
    {
        return $this->preguntas;
    }
}
