<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EncuestaRespuesta
 *
 * @ORM\Table(name="survey_encuesta_respuesta")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\EncuestaRespuestaRepository")
 */
class EncuestaRespuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="Encuesta", inversedBy="encuestaRespuestas")
     */
    private $encuesta;

    /**
     * @ORM\ManyToOne(targetEntity="EncuestaPregunta", inversedBy="encuestaRespuestas", cascade={"persist"})
     */
    private $encuestaPregunta;

    /**
     * La respuesta del usuario
     * @var string
     * @ORM\Column(name="valor", type="string", length=255)
     */
    private $valor;

    /**
     * Se usa como valor de apoyo para los campos de sentimiento
     * @var strin
     * @ORM\Column(name="ev", type="string", length=255, nullable=true)
     */
    private $ev;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return EncuestaRespuesta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set encuesta
     *
     * @param \SurveyBundle\Entity\Encuesta $encuesta
     * @return EncuestaRespuesta
     */
    public function setEncuesta(\SurveyBundle\Entity\Encuesta $encuesta = null)
    {
        $this->encuesta = $encuesta;

        return $this;
    }

    /**
     * Get encuesta
     *
     * @return \SurveyBundle\Entity\Encuesta
     */
    public function getEncuesta()
    {
        return $this->encuesta;
    }

    /**
     * Set encuestaPregunta
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $encuestaPregunta
     * @return EncuestaRespuesta
     */
    public function setEncuestaPregunta(\SurveyBundle\Entity\EncuestaPregunta $encuestaPregunta = null)
    {
        $this->encuestaPregunta = $encuestaPregunta;

        return $this;
    }

    /**
     * Get encuestaPregunta
     *
     * @return \SurveyBundle\Entity\EncuestaPregunta
     */
    public function getEncuestaPregunta()
    {
        return $this->encuestaPregunta;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return EncuestaRespuesta
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set ev
     *
     * @param string $ev
     * @return EncuestaRespuesta
     */
    public function setEv($ev)
    {
        $this->ev = $ev;

        return $this;
    }

    /**
     * Get ev
     *
     * @return string
     */
    public function getEv()
    {
        return $this->ev;
    }
}
