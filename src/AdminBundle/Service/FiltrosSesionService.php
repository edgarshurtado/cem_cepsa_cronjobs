<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\Query;
use AdminBundle\Library\FechasUtils;


/**
*
*/
class FiltrosSesionService
{

    private  $session;
    private $filtros;

    function __construct(
        Session $session, EntityManager $em, TokenStorage $ts
    ){
        $this->session = $session;
        $this->em = $em;

        $this->ts = $ts;

        $this->valoresFiltrosEnBD = $this->obtenerValoresPosiblesFiltros();

    }

    // Se usa para la obtención de datos que van a ser estáticos tras cada set
    // de filtros. Evitamos así repetición de consultas.
    public function initFiltrosService($slug)
    {
        $this->slug = $slug;

        $this->maquetaId = $this->em->getRepository("CmsBundle:Maqueta")
                                ->findOneBySlug($slug)->getId();

        // Obtenemos el ID del grupo al que pertenece el usuario loggeado
        $this->groupId = $this->ts->getToken()->getUser()
                                              ->getGroups()
                                              ->toArray()[0]
                                              ->getId();

        // Valores fijos de filtros según el grupo
        $this->valoresFijosGrupoUsuario = $this->calcularValoresFijosGrupoUser(
            $this->groupId, $this->maquetaId
        );

        // No está en el constructor porque necesitamos el $slug de la request
        $this->valoresDefaultFiltros = $this->getValoresDefaultFiltros($slug);

        // Resto de valores de filtros
        $this->valoresFiltrosMaquetaXBloque = $this->valoresBloqueMaqueta(
            $this->valoresDefaultFiltros, $this->valoresFiltrosEnBD
        );

        $this->nombreFiltros = $this->getNombreFiltros($this->valoresDefaultFiltros);

        return $this;
    }

    public function getValoresFiltroBloque($bloqueId)
    {
        return $this->valoresFiltrosMaquetaXBloque[$bloqueId];
    }

    public function getValoresEnBDFiltro($nombreEntidad)
    {
        return $this->valoresFiltrosEnBD[$nombreEntidad];
    }

    /**
     * Construye un array con los nombreId de los filtros como clave para obtener
     * el nombre real del filtro y de qué tabla proviene.
     * @param  [type] $datosFiltros [Los datos generales de los filtros de la maqueta actual]
     * @return [type]                 [description]
     */
    public function getNombreFiltros($datosFiltros){
        $r=[];
        foreach ($datosFiltros as $key => $value) {
                $r[$value['nombreId']] = array(
                    "nombreFiltro" => $value["nombreFiltro"],
                    "tabla"        => $value["tabla"]
                );
        }
        return $r;
    }

    /**
     * Obtiene los datos de los filtros
     */
    public function getValoresDefaultFiltros($slug)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select(
            "fm.nombreId, fm.valorCampoCero, fm.valorDefault as valorDefault,"
            ."f.entidadDoctrine as entidadDoctrine, f.nombreFiltro as nombreFiltro,"
            ."f.tabla as tabla, b.id as bloque, fm.tag as tag");
        $qb->from("AdminBundle:FiltrosMaqueta", "fm")
           ->leftJoin("fm.filtro", "f")
           ->join("fm.maqueta", "m")
           ->join("fm.bloque", "b")
           ->where("m.slug = :slug")
           ->orderBy("fm.orden", "ASC")
           ->setParameter("slug", $slug);

        $query = $qb->getQuery();

        $result = $query->getResult(Query::HYDRATE_ARRAY);

        return $result;
    }

    private function calcularValoresFijosGrupoUser($grupoUserFiltroId, $maquetaId){


        $db = $this->em->getConnection();

        $sql = "
            SELECT fmaqueta.nombreId nombre_filtro, valor_fijo as valor
            FROM grupo_filtro gf
            JOIN admin_filtros_maqueta fmaqueta ON gf.filtro_maqueta_id = fmaqueta.id
            WHERE gf.grupo_id = $grupoUserFiltroId
            AND fmaqueta.id_maqueta = $maquetaId
        ";

        $r = $db->executeQuery($sql)->fetchAll();

        return $r;
    }

    public function getValoresFijosGrupoUsuario()
    {
        return $this->valoresFijosGrupoUsuario;
    }

    /**
     * Función que obtiene un array con todos los datos en crudo
     * presentes en la base de datos referentes a los filtros
     *
     * Para los índices ha de usarse los nombres de los filtros que
     * aparecen en la tabla AdminBundle:Filtro
     */
    public function obtenerValoresPosiblesFiltros()
    {
        $valoresArrays = array(
            "AdminBundle:Negocio"               =>
                $this->valoresEnBDFiltro("AdminBundle:Negocio"),

            "AdminBundle:Segmento"              =>
                $this->valoresEnBDFiltro("AdminBundle:Segmento"),

            "AdminBundle:Temporalidad"          =>
                $this->valoresEnBDFiltro("AdminBundle:Temporalidad"),

            "AdminBundle:CanalEncuestacion"     =>
                $this->valoresEnBDFiltro("AdminBundle:CanalEncuestacion"),

            "AdminBundle:Vista"                 =>
                $this->valoresEnBDFiltro("AdminBundle:Vista"),

            "AdminBundle:Segmento"              =>
                $this->valoresEnBDFiltro("AdminBundle:Segmento"),

            "AdminBundle:Proceso"               =>
                $this->valoresEnBDFiltro("AdminBundle:Proceso"),

            "AdminBundle:LineaNegocio"          =>
                $this->valoresEnBDFiltro("AdminBundle:LineaNegocio"),

            "AdminBundle:Experiencia"           =>
                $this->valoresEnBDFiltro("AdminBundle:Experiencia"),

            "AdminBundle:Recomendacion"         =>
                $this->valoresEnBDFiltro("AdminBundle:Recomendacion"),

            "AdminBundle:TipologiaCliente"      =>
                $this->valoresEnBDFiltro("AdminBundle:TipologiaCliente"),

            "AdminBundle:DeterminacionCanal"    =>
                $this->valoresEnBDFiltro("AdminBundle:DeterminacionCanal"),

            "AdminBundle:Servicio"              =>
                $this->valoresEnBDFiltro("AdminBundle:Servicio"),

            "SurveyBundle:EncuestaPreguntaOpi"  =>
                $this->valoresEnBDFiltro("SurveyBundle:EncuestaPreguntaOpi"),

            "AdminBundle:Pais" =>
                $this->valoresEnBDFiltro("AdminBundle:Pais"),

            "AdminBundle:Provincia" =>
                $this->valoresEnBDFiltro("AdminBundle:Provincia"),

            "AdminBundle:Organizacion" =>
                $this->valoresEnBDFiltro("AdminBundle:Organizacion")
        );

        return $valoresArrays;
    }



    private function valoresBloqueMaqueta($defaultFiltros, $datosFiltros)
    {

        $result = [];

        foreach ($defaultFiltros as $d) {
            $bloque         = $d["bloque"];
            $nombreFiltro   = $d["entidadDoctrine"];
            $nombreId       = $d["nombreId"];
            $valorDefault   = $d["valorDefault"];
            $tag            = $d["tag"];
            $valorCampoCero = $d["valorCampoCero"] ? $d["valorCampoCero"]: "";

            if(!isset($result[$bloque])){
                $result[$bloque] = [];
            }

            $result[$bloque][$nombreId] = array(
                "nombreId" => $nombreId,
                "tag"      => $tag,
                "valores"  => isset($datosFiltros[$nombreFiltro]) ? $datosFiltros[$nombreFiltro] : [0 => ""]
            );
            if(!empty($valorCampoCero)) $result[$bloque][$nombreId]["valores"][0] = $valorCampoCero;
        }

        return $result;
    }



    public function setFiltros($slug, $filtrosEnPeticion=null)
    {
        $this->initFiltrosService($slug);
        // Comentar la línea siguiente cuando se quieran usar los filtros de
        // sesión
        // $this->session->set($slug, NULL);
        $this->filtros =    $filtrosEnPeticion +
                            (array)$this->session->get($slug) +
                            $this->newArrayFiltros($slug);

        $this->session->set($slug, $this->filtros);

        return $this;
    }

    public function getFiltros()
    {
        return $this->filtros;
    }

    /**
     * Devuelve un array con los filtros de la maqueta indicada
     */
    public function getFiltrosMaqueta($slugMaqueta){
        return $this->session->get($slugMaqueta);
    }

    /**
     * Crea la estructura del array para la maqueta proporcionada con todos
     * los valores en null
     */
    public function newArrayFiltros($slug)
    {

        $arrayEstructuraFiltros = [];

        // Seteo básico de valores a los filtros maqueta
        foreach ($this->valoresDefaultFiltros as $value) {
            if(empty($value["valorDefault"])){
                $arrayEstructuraFiltros[$value["nombreId"]] = "0";
            }else{
                $arrayEstructuraFiltros[$value["nombreId"]] = $value["valorDefault"];
            }
        }

        // Asignación Valores Fijos
        $valoresFijos = $this->valoresFijosGrupoUsuario;

        foreach ($valoresFijos as $filtro) {
            if(!is_null($filtro["valor"]))
                $arrayEstructuraFiltros[$filtro["nombre_filtro"]] = $filtro["valor"];
        }

        // Seteo de fechas por defecto
        if( isset($arrayEstructuraFiltros["temporalidad"]) )
        {
            // Seteo fechas para cuando temporalidad es por Rango
            if($arrayEstructuraFiltros["temporalidad"] == "0")
            {
                $fechasRangoDefault = FechasUtils::getRangoTemporalidad("semana");

                $arrayEstructuraFiltros["fec_ini"] =
                    $fechasRangoDefault["fecha_inicio"]->format("Y-m-d");

                $arrayEstructuraFiltros["fec_fin"] =
                    $fechasRangoDefault["fecha_fin"]->format("Y-m-d");
            }else{
            // Seteo las fechas cuando la temporalidad no es de rango
            // pero estan definidas una o ambas fechas.
                $fechasRangoDefault = FechasUtils::getRangoTemporalidad("dia");
                // Seteo solo si estan definidas
                if( isset($arrayEstructuraFiltros['fec_ini']) )
                    $arrayEstructuraFiltros["fec_ini"] =
                        $fechasRangoDefault["fecha_inicio"]->format("Y-m-d");
                if( isset($arrayEstructuraFiltros['fec_ini']) )
                    $arrayEstructuraFiltros["fec_fin"] =
                        $fechasRangoDefault["fecha_fin"]->format("Y-m-d");
            }
        }

        return $arrayEstructuraFiltros;
    }

    public function valoresEnBDFiltro($entity){
        $qb = $this->em->createQueryBuilder();

        $qb->select("f.id as id, f.nombre as valor")
           ->from($entity, "f");

        $query = $qb->getQuery();

        $queryResult = $query->getResult(Query::HYDRATE_ARRAY);

        $funtionResult = [];

        foreach ($queryResult as $registroFiltro) {
            $functionResult[$registroFiltro["id"]] = $registroFiltro["valor"];
        }

        return $functionResult;
    }

    public function setFiltro($nombreFiltro, $nombreMaqueta, $valor)
    {
        $this->filtros[$nombreFiltro] = $valor;

        $this->session->set($nombreMaqueta, $this->filtros);
    }
}
