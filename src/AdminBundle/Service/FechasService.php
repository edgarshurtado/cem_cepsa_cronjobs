<?php
namespace AdminBundle\Service;
use Doctrine\ORM\EntityManager;
use AdminBundle\Service\FiltrosSesionService as FiltrosSesion;

class FechasService {

    function __construct(EntityManager $em, FiltrosSesion $fs){

        $this->valoresTemporalidad =
            $fs->getValoresEnBDFiltro("AdminBundle:Temporalidad");
    }

    public function getFechaUHoy($nombreFiltro)
    {
        if(isset($this->filtros[$nombreFiltro])){
            return new \DateTime($this->filtros[$nombreFiltro]);
        }else{
            return new \DateTime();
        }
    }

    public function getRangoTemporalidadId(
        $temporalidadId, \DateTime $fecha_ref=null, $periodos=1
    ){
        $temporalidadString = $this->valoresTemporalidad[$temporalidadId];

        return $this->getRangoTemporalidad(
            $temporalidadString, $fecha_ref, $periodos
        );
    }


    /**
    *   Devuelve un array con fechas inicio y fin para una temporalidad
    *   @temporalidad, semana,mes,año,... (para los ids se ha hecho una funcion especifica) ...ID
    *   @fecha_ref, (OPCIONAL) Datetime (formato YYYY-mm-dd)
    *   @periodos, (OPCIONAL) Numero de temporalidades a obtener fechas, por defecto un periodo(el actual)
    */
    public function getRangoTemporalidad($temporalidad, \DateTime $fecha_ref=null, $periodos=1)
    {
        //Si la fecha no existe o no es objeto DateTime, la genero
        if($fecha_ref==null){
            $fecha_ref = new \DateTime();
        }
        if(!($fecha_ref instanceof \DateTime)){
            $fecha_ref = new \DateTime($fecha_ref);
        }

        $fechaActual = $fecha_ref;
        $mesActual = intval($fechaActual->format("m"));
        $mesInicio = intval($fechaActual->format("m"))-($periodos-1);
        $anyoActual = intval($fechaActual->format("Y"));
        $anyoInicio = intval($fechaActual->format("Y"))-($periodos-1);

        $fechaInicio = new \DateTime();
        $fechaFin = new \DateTime();

        switch ($temporalidad) {
            case 'dia':
                break;
            case 'semana':
                $semanaActual = intval($fechaActual->format("W"));
                $semanaInicio = intval($fechaActual->format("W"))-($periodos-1);
                $fechaInicio->setISODate($anyoActual, $semanaInicio, 1);
                $fechaFin->setISODate($anyoActual, $semanaInicio, 7);
                break;
            case 'mes':
                $maximoDiaMesActual = intval($fechaActual->format("t"));
                $fechaInicio->setDate($anyoActual, $mesInicio, 1);
                $maximoDiaMesPeriodo = intval($fechaInicio->format("t"));
                $fechaFin->setDate($anyoActual, $mesInicio, $maximoDiaMesPeriodo);
                break;
            case 'año':
                $diciembre = 12;
                $nDiasDiciembre = 31;
                $fechaInicio->setDate($anyoInicio, 1, 1);
                $fechaFin->setDate($anyoInicio, $diciembre, $nDiasDiciembre);
                break;
            case 'trimestre':
                $fechaRefTrimestre = new \DateTime();
                $mesInicio = $mesActual-($periodos*3)+3;//Mes contenido en el trimestre

                $fechaRefTrimestre->setDate($anyoActual, $mesInicio, 1);

                $rangoTrimestre = FechasUtils::getRangoTrimestre($fechaRefTrimestre);
                $fechaInicio = $rangoTrimestre["fecha_inicio"];

                $fechaFin = $rangoTrimestre["fecha_fin"];
                break;
            default:
                break;
        }

        $fechaInicio->setTime(0,0,0);
        $fechaFin->setTime(23,59,59);

        return array(
            "fecha_inicio" => $fechaInicio,
            "fecha_fin"    => $fechaFin
        );
    }

    /**
     * Funcion que devuelve un array con el primer y último dia
     * del trimestre de la fecha pasada como parametro.
     * @param type $datetime
     * @return array
     */
    public function getRangoTrimestre($datetime) {

        $anyo = intval($datetime->format("Y"));
        //$trimestre = $this->getTrimestre($datetime);
        $trimestre = floor(($datetime->format('m') - 1) / 3 + 1);

        $fechaInicio = new \DateTime();
        $fechaFin = new \DateTime();

        switch ($trimestre) {
            case 1:
                $fechaInicio->setDate($anyo, 1, 1);
                $fechaFin->setDate($anyo, 3, 31);
                break;
            case 2:
                $fechaInicio->setDate($anyo, 4, 1);
                $fechaFin->setDate($anyo, 6, 30);
                break;
            case 3:
                $fechaInicio->setDate($anyo, 7, 1);
                $fechaFin->setDate($anyo, 9, 30);
                break;
            case 4:
                $fechaInicio->setDate($anyo, 10, 1);
                $fechaFin->setDate($anyo, 12, 31);
                break;
        }

        //No seteo las horas, eso lo hace la funcion padre (getRangoTemporalidad)
        //Si se usa esta funcion independientemente, habrìa que setear esas horas.

        return array(
            "fecha_inicio" => $fechaInicio,
            "fecha_fin"    => $fechaFin
        );
    }

    /**
     * Devuelve el trimestre para una fecha (1,2,3,4)
     * @param type $datetime
     * @return int
     */
    public function getTrimestre($datetime) {
        $mes = intval($datetime->format("m"));
        //$mes = date("m", strtotime($datetime->format("Y-m-d")));
        $mes = is_null($mes) ? date('m') : $mes;
        $trimestre = floor(($mes - 1) / 3) + 1;

        return $trimestre;
    }
    /*
    * Funcion que devuelve un array de fechas inicio y fin para cada periodo comprendido entre
    * el que se especifique y el actual.
    */
    public function getRangosTemporalidad($temporalidadString, \DateTime $fecha_ref, $periodos=1){
        $arrayFechasCompleto=[];
        for ($i=1; $i <= $periodos ; $i++) {
            $arrayFechasCompleto[$i]=$this->getRangoTemporalidad($temporalidadString, $fecha_ref);
            $fecha_ref = $arrayFechasCompleto[$i]["fecha_inicio"]->sub(new \DateInterval('P1D'));
        }
        return $arrayFechasCompleto;
    }

    /*
        Devuelve el nombre asociado a la temporalidad y a una fecha.
        Si no se especifica fecha (Datetime), usa como referencia la actual
    */
    public function getNombreMostrarTemporalidad($temporalidad,$fecha=null){
        if($fecha==null){
            $fecha = new \DateTime();
        }
        if(!($fecha instanceof \DateTime)){
            $fecha = new \DateTime($fecha);
        }

        switch ($temporalidad) {
                case 'dia':
                    $nombre = ucfirst(('día')) . ":\n " . $fecha->format("d-m-Y");
                    break;
                case 'semana':
                    $fechaLunes = new \DateTime();
                    $fechaLunes = $fechaLunes->setISODate(intval($fecha->format("Y")), intval($fecha->format("W")), 1)->format('d/m/Y');
                    $nombre = "Sem".$fecha->format('W') . ":\n " . $fechaLunes;
                    break;
                case 'mes':
                    $fecha = ucfirst(strftime("%b/%Y", $fecha->getTimeStamp()));
                    $nombre = "Mes" . ": " . $fecha;
                    break;
                case 'año':
                    $nombre = "Año" . ":\n " . $fecha->format('Y');
                    break;
                case 'trimestre':
                    $nombre = "Trimestre" . ":\n T" . floor(($fecha->format('m') - 1) / 3 + 1) . '-' . $fecha->format('Y');
                    break;
                default:
                    $nombre = "";
        }
        return $nombre;
    }

    /**
     * Funcion que devuelve el el periodo de la temporalidad en funcion de la fecha
     * @param type $cruce
     * @return int
     */
    public static function getPeriodo($temporalidad,$fecha=null) {
        if($fecha==null){
            $fecha = new \DateTime();
        }
        if(!($fecha instanceof \DateTime)){
            $fecha = new \DateTime($fecha);
        }

        switch ($temporalidad) {
                case 'dia':
                    $periodo = $fecha->format('d');
                    break;
                case 'semana':
                    $periodo = $fecha->format('W');
                    break;
                case 'mes':
                    $periodo = $fecha->format('m');
                    break;
                case 'año':
                    $periodo = $fecha->format('Y');
                    break;
                case 'trimestre':
                    $periodo = floor(($fecha->format('m') - 1) / 3 + 1);
                    break;
                default:
                    $periodo = "1";
        }
        return $periodo;
    }
    /**
     * Funcion que devuelve el numero de temporalidades a las que nos vamos a remontar
     * @param type $cruce
     * @return int
     */
    public static function getNumPeriodos($temporalidad) {
        $periodo = 0;
        switch ($temporalidad) {
            case 'dia':
                $periodo = 7;
                break;
            case 'semana':
                $periodo = 8;
                break;
            case 'mes':
                $periodo = 6;
                break;
            case 'año':
                $periodo = 4;
                break;
            case 'trimestre':
                $periodo = 8;
                break;
            default:
                $periodo = 1;
                break;
        }
        return $periodo;
    }

}