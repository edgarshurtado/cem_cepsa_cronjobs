<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCem
 *
 * @ORM\Table(name="admin_tipo_cem")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\TipoCemRepository")
 */
class TipoCem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\EncuestaPregunta", mappedBy="cemTipo")
     */
    private $preguntas;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoCem
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add preguntas
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $preguntas
     * @return TipoCem
     */
    public function addPregunta(\SurveyBundle\Entity\EncuestaPregunta $preguntas)
    {
        $this->preguntas[] = $preguntas;

        return $this;
    }

    /**
     * Remove preguntas
     *
     * @param \SurveyBundle\Entity\EncuestaPregunta $preguntas
     */
    public function removePregunta(\SurveyBundle\Entity\EncuestaPregunta $preguntas)
    {
        $this->preguntas->removeElement($preguntas);
    }

    /**
     * Get preguntas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPreguntas()
    {
        return $this->preguntas;
    }
}
