<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Negocio
 *
 * @ORM\Table(name="admin_negocio")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\NegocioRepository")
 */
class Negocio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="neg")
     */
    private $encuestas;

    /**
     * @ORM\OneToMany(targetEntity="NegocioLineaNegocio", mappedBy="negocio")
     */
    private $negocioLineaNegocio;

    /**
     * @ORM\OneToMany(targetEntity="NegocioProducto", mappedBy="negocio")
     */
    private $negocioProductos;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->NegocioLineaNegocio = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Negocio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return Negocio
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }

    /**
     * Add NegocioLineaNegocio
     *
     * @param \AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio
     * @return Negocio
     */
    public function addNegocioLineaNegocio(\AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio)
    {
        $this->NegocioLineaNegocio[] = $negocioLineaNegocio;

        return $this;
    }

    /**
     * Remove NegocioLineaNegocio
     *
     * @param \AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio
     */
    public function removeNegocioLineaNegocio(\AdminBundle\Entity\NegocioLineaNegocio $negocioLineaNegocio)
    {
        $this->NegocioLineaNegocio->removeElement($negocioLineaNegocio);
    }

    /**
     * Get NegocioLineaNegocio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNegocioLineaNegocio()
    {
        return $this->NegocioLineaNegocio;
    }

    /**
     * Add negocioProductos
     *
     * @param \AdminBundle\Entity\NegocioProducto $negocioProductos
     * @return Negocio
     */
    public function addNegocioProducto(\AdminBundle\Entity\NegocioProducto $negocioProductos)
    {
        $this->negocioProductos[] = $negocioProductos;

        return $this;
    }

    /**
     * Remove negocioProductos
     *
     * @param \AdminBundle\Entity\NegocioProducto $negocioProductos
     */
    public function removeNegocioProducto(\AdminBundle\Entity\NegocioProducto $negocioProductos)
    {
        $this->negocioProductos->removeElement($negocioProductos);
    }

    /**
     * Get negocioProductos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNegocioProductos()
    {
        return $this->negocioProductos;
    }
}
