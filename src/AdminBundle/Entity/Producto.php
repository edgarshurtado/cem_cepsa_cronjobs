<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Producto
 *
 * @ORM\Table(name="admin_producto")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\ProductoRepository")
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="NegocioProducto", mappedBy="producto")
     */
    private $negocioProductos;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->negocioProductos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add negocioProductos
     *
     * @param \AdminBundle\Entity\NegocioProducto $negocioProductos
     * @return Producto
     */
    public function addNegocioProducto(\AdminBundle\Entity\NegocioProducto $negocioProductos)
    {
        $this->negocioProductos[] = $negocioProductos;

        return $this;
    }

    /**
     * Remove negocioProductos
     *
     * @param \AdminBundle\Entity\NegocioProducto $negocioProductos
     */
    public function removeNegocioProducto(\AdminBundle\Entity\NegocioProducto $negocioProductos)
    {
        $this->negocioProductos->removeElement($negocioProductos);
    }

    /**
     * Get negocioProductos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNegocioProductos()
    {
        return $this->negocioProductos;
    }
}
