<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NegocioLineaNegocio
 *
 * @ORM\Table(name="negocio_linea_negocio")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\NegocioLineaNegocioRepository")
 */
class NegocioLineaNegocio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Negocio", inversedBy="negocioLineaNegocio")
     */
    private $negocio;

    /**
     * @ORM\ManyToOne(targetEntity="LineaNegocio", inversedBy="negocioLineaNegocio")
     */
    private $lineaNegocio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set negocio
     *
     * @param \AdminBundle\Entity\Negocio $negocio
     * @return NegocioLineaNegocio
     */
    public function setNegocio(\AdminBundle\Entity\Negocio $negocio = null)
    {
        $this->negocio = $negocio;

        return $this;
    }

    /**
     * Get negocio
     *
     * @return \AdminBundle\Entity\Negocio
     */
    public function getNegocio()
    {
        return $this->negocio;
    }

    /**
     * Set lineaNegocio
     *
     * @param \AdminBundle\Entity\LineaNegocio $lineaNegocio
     * @return NegocioLineaNegocio
     */
    public function setLineaNegocio(\AdminBundle\Entity\LineaNegocio $lineaNegocio = null)
    {
        $this->lineaNegocio = $lineaNegocio;

        return $this;
    }

    /**
     * Get lineaNegocio
     *
     * @return \AdminBundle\Entity\LineaNegocio 
     */
    public function getLineaNegocio()
    {
        return $this->lineaNegocio;
    }
}
