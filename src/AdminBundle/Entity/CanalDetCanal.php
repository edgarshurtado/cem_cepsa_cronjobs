<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CanalDetCanal
 *
 * @ORM\Table(name="admin_canal_det_canal")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\CanalDetCanalRepository")
 */
class CanalDetCanal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CanalEncuestacion", inversedBy="canalDetCanal")
     */
    private $canal;

    /**
     * @ORM\ManyToOne(targetEntity="DeterminacionCanal", inversedBy="canalDetCanal")
     */
    private $determinacionCanal;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set canal
     *
     * @param \AdminBundle\Entity\CanalEncuestacion $canal
     * @return CanalDetCanal
     */
    public function setCanal(\AdminBundle\Entity\CanalEncuestacion $canal = null)
    {
        $this->canal = $canal;

        return $this;
    }

    /**
     * Get canal
     *
     * @return \AdminBundle\Entity\Canal
     */
    public function getCanal()
    {
        return $this->canal;
    }

    /**
     * Set determinacionCanal
     *
     * @param \AdminBundle\Entity\DeterminacionCanal $determinacionCanal
     * @return CanalDetCanal
     */
    public function setDeterminacionCanal(\AdminBundle\Entity\DeterminacionCanal $determinacionCanal = null)
    {
        $this->determinacionCanal = $determinacionCanal;

        return $this;
    }

    /**
     * Get determinacionCanal
     *
     * @return \AdminBundle\Entity\DeterminacionCanal
     */
    public function getDeterminacionCanal()
    {
        return $this->determinacionCanal;
    }
}
