<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipificacionOpinion
 *
 * @ORM\Table(name="admin_tipificacion_opinion")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\TipificacionOpinionRepository")
 */
class TipificacionOpinion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * Este no se puede hacer aún porque hay en encuestas tip1 tip2 y tip3.
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="tip1")
     */
    private $encuestas;

    /**
     * Este no se puede hacer aún porque hay en encuestas tip1 tip2 y tip3.
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="tip2")
     */
    private $encuestas2;

    /**
     * Este no se puede hacer aún porque hay en encuestas tip1 tip2 y tip3.
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="tip3")
     */
    private $encuestas3;

    /**
     * Este no se puede hacer aún porque hay en encuestas tip1 tip2 y tip3.
     * @ORM\OneToMany(targetEntity="SurveyBundle\Entity\Encuesta", mappedBy="tip4")
     */
    private $encuestas4;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipificacionOpinion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     * @return TipificacionOpinion
     */
    public function addEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas[] = $encuestas;

        return $this;
    }

    /**
     * Remove encuestas
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas
     */
    public function removeEncuesta(\SurveyBundle\Entity\Encuesta $encuestas)
    {
        $this->encuestas->removeElement($encuestas);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }

    /**
     * Add encuestas2
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas2
     * @return TipificacionOpinion
     */
    public function addEncuestas2(\SurveyBundle\Entity\Encuesta $encuestas2)
    {
        $this->encuestas2[] = $encuestas2;

        return $this;
    }

    /**
     * Remove encuestas2
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas2
     */
    public function removeEncuestas2(\SurveyBundle\Entity\Encuesta $encuestas2)
    {
        $this->encuestas2->removeElement($encuestas2);
    }

    /**
     * Get encuestas2
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEncuestas2()
    {
        return $this->encuestas2;
    }

    /**
     * Add encuestas3
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas3
     * @return TipificacionOpinion
     */
    public function addEncuestas3(\SurveyBundle\Entity\Encuesta $encuestas3)
    {
        $this->encuestas3[] = $encuestas3;

        return $this;
    }

    /**
     * Remove encuestas3
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas3
     */
    public function removeEncuestas3(\SurveyBundle\Entity\Encuesta $encuestas3)
    {
        $this->encuestas3->removeElement($encuestas3);
    }

    /**
     * Get encuestas3
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEncuestas3()
    {
        return $this->encuestas3;
    }

    /**
     * Add encuestas4
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas4
     * @return TipificacionOpinion
     */
    public function addEncuestas4(\SurveyBundle\Entity\Encuesta $encuestas4)
    {
        $this->encuestas4[] = $encuestas4;

        return $this;
    }

    /**
     * Remove encuestas4
     *
     * @param \SurveyBundle\Entity\Encuesta $encuestas4
     */
    public function removeEncuestas4(\SurveyBundle\Entity\Encuesta $encuestas4)
    {
        $this->encuestas4->removeElement($encuestas4);
    }

    /**
     * Get encuestas4
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEncuestas4()
    {
        return $this->encuestas4;
    }
}
