<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * FiltrosBloqueRepository
 *
 */
class FiltrosBloqueRepository extends EntityRepository
{
    public function getValoresFiltrosBloque($bloqueId)
    {
        $datosFiltrosMaqueta =
            $this->getFiltrosMaquetaDeBloque($bloqueId);

        $resultado = [];

        foreach ($datosFiltrosMaqueta as $filtroMaqueta) {
            $resultado[$filtroMaqueta["nombre"]] = array(
                "nombreId" => $filtroMaqueta["nombre"],
                "tag"      => $filtroMaqueta["tag"],
                "valores"  => $this->getValoresFiltro($filtroMaqueta)
            );
        }
        var_dump("Deprecated!!");exit();
        return $resultado;
    }

    private function getFiltrosMaquetaDeBloque($bloqueId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $select = "fm.nombreId as nombre, fm.valorDefault, fm.valorCampoCero, fm.tag, f.entidadDoctrine";

        $qb->select($select)
           ->from("AdminBundle:FiltrosMaqueta", "fm")
           ->join("fm.bloque", "b", "WITH", "b.id = :bloqueId")
           ->join("fm.filtro", "f")
           ->setParameter("bloqueId", $bloqueId);

        $query = $qb->getQuery();
        $resultado = $query->getResult(Query::HYDRATE_ARRAY);

        return $resultado;
    }

    public function getValoresFiltro($filaFiltrosMaqueta)
    {

        $campoCero = $filaFiltrosMaqueta["valorCampoCero"];

        $resultado = [];

        if($filaFiltrosMaqueta["valorDefault"] === "0"){
            $resultado[] = $campoCero;
        }

        $entidadDoctrine = $filaFiltrosMaqueta["entidadDoctrine"];

        $qb = $this->getEntityManager()->createQueryBuilder();

        $select = "f.id, f.nombre";

        $qb->select($select)
           ->from($entidadDoctrine, "f");

        $query = $qb->getQuery();

        $resultados = $query->getResult(Query::HYDRATE_ARRAY);

        foreach ($resultados as $valor) {
            $resultado[$valor["id"]] = $valor["nombre"];
        }

        return $resultado;
    }

    public function getValoresFiltroPorNombre($nombreIdFiltro)
    {
        $filaFiltrosMaqueta = $this->getEntityManager()
                                   ->getRepository("AdminBundle:FiltrosMaqueta")
                                   ->findOneByNombreId($nombreIdFiltro);


        $qb = $this->getEntityManager()->createQueryBuilder();

        $select = "fm.nombreId as nombre, fm.valorDefault, fm.valorCampoCero, fm.tag, f.entidadDoctrine";

        $qb->select($select)
           ->from("AdminBundle:FiltrosMaqueta", "fm")
           ->join("fm.filtro", "f")
           ->where("fm.nombreId = :nombreIdFiltro")
           ->setParameter("nombreIdFiltro", $nombreIdFiltro);

        $query = $qb->getQuery();
        $resultado = $query->getResult(Query::HYDRATE_ARRAY);

        return $this->getValoresFiltro($resultado[0]);
    }
}
