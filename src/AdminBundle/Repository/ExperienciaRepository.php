<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ExperienciaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ExperienciaRepository extends EntityRepository
{
}
