<?php

namespace Madison\LogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MadisonLogBundle:Default:index.html.twig');
    }
}
