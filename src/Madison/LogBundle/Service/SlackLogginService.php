<?php
namespace Madison\LogBundle\Service;

use Monolog\Logger;
use Monolog\Handler\SlackHandler;
use Monolog\Formatter\LineFormatter;

/**
*
*/
class SlackLogginService
{

    function __construct($OATH, $channel, $botName)
    {
        $this->logger = new Logger("SlackLogger");
        $this->slackHandler = new SlackHandler($OATH, $channel, $botName);
        $this->slackHandler->setFormatter(new LineFormatter(null, null, true));

        $this->logger->pushHandler($this->slackHandler);
    }

    public function error($msg)
    {
        $this->slackHandler->setLevel(\Monolog\Logger::ERROR);
        $this->logger->error($msg);
    }

    public function info($msg)
    {
        $this->slackHandler->setLevel(\Monolog\Logger::INFO);
        $this->logger->info($msg);
    }

    public function alert($msg)
    {
        $this->slackHandler->setLevel(\Monolog\Logger::ALERT);
        $this->logger->alert($msg);
    }

    public function warning($msg)
    {
        $this->slackHandler->setLevel(\Monolog\Logger::WARNING);
        $this->logger->warning($msg);
    }
}