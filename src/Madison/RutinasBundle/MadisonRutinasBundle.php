<?php

namespace Madison\RutinasBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Console\Application;
use Madison\RutinasBundle\Command\PoblarPermutacionesVistas;
use Madison\RutinasBundle\Command\PoblarEncuestaProcesos;
use Madison\RutinasBundle\Command\BorrarEncuestas;
use Madison\RutinasBundle\Command\PoblarEncuestasDatatable;
use Madison\RutinasBundle\Command\PoblarNosqlEncuestas;
use Madison\RutinasBundle\Command\PoblarNosqlEncuestaProcesos;

class MadisonRutinasBundle extends Bundle
{
    /**
     * Registra de manera automática el comando para que esté accesible desde
     * app/console
     */
    public function registerCommands(Application $application)
    {
        $application->add(new PoblarPermutacionesVistas());
        $application->add(new PoblarEncuestaProcesos());
        $application->add(new PoblarEncuestasDatatable());
        $application->add(new PoblarNosqlEncuestas());
        $application->add(new PoblarNosqlEncuestaProcesos());
        $application->add(new BorrarEncuestas());
    }
}
