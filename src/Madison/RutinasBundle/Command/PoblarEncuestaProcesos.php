<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption; // Para aceptar opciones por
                                                   // linea de comandos

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

// Entidad doctrine en la que persistir las nuevas permutaciones encontradas
// en la bd
use SurveyBundle\Entity\EncuestaProceso;

// Librería de Fechas
use AdminBundle\Library\FechasUtils;

/*ALERTA CG!
Esta clase es un duplicado de la clase para generar permutaciones. Habría que
buscar una forma genérica para pasarle SQL de búsqueda, inserción y comprobación
*/
class PoblarEncuestaProcesos extends ContainerAwareCommand
{
    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:poblar-encuesta-procesos') // Nombre para su
                                                         // ejecución
            ->setDescription('Genera en la base de datos las combinaciones de
                encuestas con procesos')
            ->addOption(
                "rango",
                "t",
                InputOption::VALUE_NONE,
                "Leer toda la base de datos?"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Preparación de los objetos doctrine a utilizar
        $this->em = $this->getContainer()
                         ->get("doctrine")
                         ->getManager();

        $dql = $this->getContainer()
                    ->get("admin.dql");

        // Preparación de las variables para dar feedback al usuario
        $tiempoInicio = new \DateTime();
        $tiempoInicio = $tiempoInicio->getTimestamp();
        $filasInsertadas = 0;
        $filasProcesadas = 0;

        // el JOIN con encuesta es necesario para poder hacer la delimitacion
        // por fechas.
        $sql = "
            SELECT DISTINCT r.encuesta_id encuesta, p.sub_bloque_id proceso
            FROM survey_encuesta_respuesta r
            JOIN survey_encuesta e ON e.id = r.encuesta_id
            JOIN survey_encuesta_pregunta p ON p.id = r.encuesta_pregunta_id
            WHERE p.cem_tipo_id IS NOT NULL
                AND p.sub_bloque_id > 1 AND p.sub_bloque_id <13
        ";

        $procesarTodo = $input->getOption("rango");
        if(!$procesarTodo){
            // Con el código a continuación se comprueba desde el mes pasado
            // hasta el actual (Para evitar conflictos en la carga de un día 31
            // en el día 1 o 2 del mes siguiente)
            $temporalidadIdMes = 2;
            $periodos = 2;
            $aliasTablaFiltrado = "e";

            $rangoFechas = FechasUtils::getRangoTemporalidadId(
                $temporalidadIdMes, new \DateTime(), $periodos
            );

            $sql = $dql->sqlAddRangoFechas(
                $aliasTablaFiltrado, $rangoFechas["fecha_inicio"],
                $rangoFechas["fecha_fin"], $sql
            );
        }

        // Iteración sobre los resultados que devuelve el generador
        foreach($dql->generadorResultadosSQL($sql) as $row){

            if($this->filaEnBD($row)) continue; // En caso de que la combinación
                                                // ya se encuentre en la BD.

            $this->em->persist($this->creaObjeto($row)); // Adición de la nueva
                                                         // fila a la cola de
                                                         // persistencia.

            if($filasInsertadas % 100 == 0) // Persistir por paquetes
            {
                $this->em->flush();
                $this->em->clear();
            }

            $filasInsertadas++;
        }
        // Persistir remanente
        $this->em->flush();
        $this->em->clear();


        // Feedback al usuario
        $tiempoEjecucion = new \DateTime();
        $tiempoEjecucion = $tiempoEjecucion->getTimestamp() - $tiempoInicio;

        $output->writeln("Se han insertado " . $filasInsertadas . " filas.");
        $output->writeln("proceso realizado en: " . $tiempoEjecucion . " ms.");
    }


    private function filaEnBD($fila)
    {
        $objEnBD = $this->em
                        ->getRepository("SurveyBundle:EncuestaProceso")
                        ->findOneBy(array(
                            "encuesta" => $fila["encuesta"],
                            "proceso" => $fila["proceso"]
                        ));

        return !is_null($objEnBD);
    }


    private function creaObjeto($fila)
    {
        $obj = new EncuestaProceso();

        $obj->setEncuesta($this->getObjById(
            "SurveyBundle:Encuesta" , $fila["encuesta"])
        );

        $obj->setProceso($this->getObjById(
            "AdminBundle:Proceso" , $fila["proceso"])
        );

        return $obj;
    }

    private function getObjById($repositoryName, $idValue){
        return $this->em->getRepository($repositoryName)->findOneById($idValue);
    }
}
