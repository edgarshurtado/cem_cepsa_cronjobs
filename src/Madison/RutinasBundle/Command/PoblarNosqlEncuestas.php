<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption; // Para aceptar opciones por
                                                   // linea de comandos

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

/**
*
*/
class PoblarNosqlEncuestas extends ContainerAwareCommand
{

    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:poblar-nosql-encuestas') // Nombre para su ejecución
            ->setDescription('Generación tabla nosql-encuestas');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get("doctrine")->getManager();

        $conn = $em->getConnection();

        $sql = "
        INSERT INTO nosql_encuestas (
            encuesta_id, fecha, pais_id, opi_pregunta_id, opi_id,
            seg_id, neg_id, org_id, can_id, det_can_id, ser1_id,
            g1, g1_valor, g2, g3, a1, gc1, gc2, gc3, gc4, of1, of2, of3, of4,
            p2, pc1, pc2, ps1, ps2, e1, e2, e3, e4
        )

        SELECT
            e.id,
            e.fecha as fecha,
            e.pais_id as pais_id,
            e.opi_pregunta_id as opi_pregunta_id,
            e.opi_id as opi_id,
            e.seg_id as segmento_id,
            e.neg_id as negocio_id,
            e.org_id as linea_negocio_id,
            e.can_id as canal_id,
            e.det_can as detalle_canal_id,
            e.ser1_id as servicio_id,
            (
                SELECT g1.valor
                    FROM survey_encuesta_respuesta g1
                    WHERE g1.encuesta_pregunta_id = 1
                    AND g1.encuesta_id = e.id

            ) as 'g1',
            (
                SELECT g1.ev
                    FROM survey_encuesta_respuesta g1
                    WHERE g1.encuesta_pregunta_id = 1
                    AND g1.encuesta_id = e.id

            ) as 'g1_valor',
            (
                SELECT g2.valor
                    FROM survey_encuesta_respuesta g2
                    WHERE g2.encuesta_pregunta_id = 2
                    AND g2.encuesta_id = e.id

            ) as 'g2',
            (
                SELECT g3.valor
                    FROM survey_encuesta_respuesta g3
                    WHERE g3.encuesta_pregunta_id = 3
                    AND g3.encuesta_id = e.id

            ) as 'g3',
            (
                SELECT a1.valor
                    FROM survey_encuesta_respuesta a1
                    WHERE a1.encuesta_pregunta_id = 15
                    AND a1.encuesta_id = e.id

            ) as 'a1',
            (
                SELECT gc1.valor
                    FROM survey_encuesta_respuesta gc1
                    WHERE gc1.encuesta_pregunta_id = 23
                    AND gc1.encuesta_id = e.id

            ) as 'gc1',
            (
                SELECT gc2.valor
                    FROM survey_encuesta_respuesta gc2
                    WHERE gc2.encuesta_pregunta_id = 25
                    AND gc2.encuesta_id = e.id

            ) as 'gc2',
            (
                SELECT gc3.valor
                    FROM survey_encuesta_respuesta gc3
                    WHERE gc3.encuesta_pregunta_id = 26
                    AND gc3.encuesta_id = e.id

            ) as 'gc3',
            (
                SELECT gc4.valor
                    FROM survey_encuesta_respuesta gc4
                    WHERE gc4.encuesta_pregunta_id = 8
                    AND gc4.encuesta_id = e.id

            ) as 'gc4',
            (
                SELECT of1.valor
                    FROM survey_encuesta_respuesta of1
                    WHERE of1.encuesta_pregunta_id = 5
                    AND of1.encuesta_id = e.id

            ) as 'of1',
            (
                SELECT of2.valor
                    FROM survey_encuesta_respuesta of2
                    WHERE of2.encuesta_pregunta_id = 6
                    AND of2.encuesta_id = e.id

            ) as 'of2',
            (
                SELECT of3.valor
                    FROM survey_encuesta_respuesta of3
                    WHERE of3.encuesta_pregunta_id = 4
                    AND of3.encuesta_id = e.id

            ) as 'of3',
            (
                SELECT of4.valor
                    FROM survey_encuesta_respuesta of4
                    WHERE of4.encuesta_pregunta_id = 22
                    AND of4.encuesta_id = e.id

            ) as 'of4',
            (
                SELECT p2.valor
                    FROM survey_encuesta_respuesta p2
                    WHERE p2.encuesta_pregunta_id = 7
                    AND p2.encuesta_id = e.id

            ) as 'p2',
            (
                SELECT pc1.valor
                    FROM survey_encuesta_respuesta pc1
                    WHERE pc1.encuesta_pregunta_id = 20
                    AND pc1.encuesta_id = e.id

            ) as 'pc1',
            (
                SELECT pc2.valor
                    FROM survey_encuesta_respuesta pc2
                    WHERE pc2.encuesta_pregunta_id = 24
                    AND pc2.encuesta_id = e.id

            ) as 'pc2',
            (
                SELECT ps1.valor
                    FROM survey_encuesta_respuesta ps1
                    WHERE ps1.encuesta_pregunta_id = 9
                    AND ps1.encuesta_id = e.id

            ) as 'ps1',
            (
                SELECT ps2.valor
                    FROM survey_encuesta_respuesta ps2
                    WHERE ps2.encuesta_pregunta_id = 21
                    AND ps2.encuesta_id = e.id

            ) as 'ps2',
            (
                SELECT e1.valor
                    FROM survey_encuesta_respuesta e1
                    WHERE e1.encuesta_pregunta_id = 5
                    AND e1.encuesta_id = e.id

            ) as 'e1',
            (
                SELECT e2.valor
                    FROM survey_encuesta_respuesta e2
                    WHERE e2.encuesta_pregunta_id = 6
                    AND e2.encuesta_id = e.id

            ) as 'e2',
            (
                SELECT e3.valor
                    FROM survey_encuesta_respuesta e3
                    WHERE e3.encuesta_pregunta_id = 4
                    AND e3.encuesta_id = e.id

            ) as 'e3',
            (
                SELECT e4.valor
                    FROM survey_encuesta_respuesta e4
                    WHERE e4.encuesta_pregunta_id = 22
                    AND e4.encuesta_id = e.id

            ) as 'e4'
        FROM survey_encuesta e
            JOIN survey_encuesta_pregunta_opi opi_pregunta ON opi_pregunta.id = e.opi_pregunta_id
            JOIN admin_segmento ON admin_segmento.id = e.seg_id
            JOIN admin_negocio ON admin_negocio.id = e.neg_id
            JOIN admin_linea_negocio ON admin_linea_negocio.id = e.org_id
            JOIN admin_canal_encuestacion ON admin_canal_encuestacion.id = e.can_id
            JOIN admin_determinacion_canal ON admin_determinacion_canal.id = e.det_can
            JOIN admin_servicio ON admin_servicio.id = e.ser1_id
        WHERE e.id NOT IN (
            SELECT encuesta_id FROM nosql_encuestas
        )
           ";

        $conn->executeQuery($sql);

        // Columna warning
        $sql = "
        UPDATE nosql_encuestas set warning =
            (SELECT IF(SUM(IF(er_warning.valor < 5, 1,0)) = 0, 0, 1) as warning
            FROM `survey_encuesta_respuesta` er_warning
              JOIN `survey_encuesta_pregunta` ep_warning ON er_warning.encuesta_pregunta_id = ep_warning.id
              WHERE ep_warning.cem_tipo_id = 4 && er_warning.encuesta_id = nosql_encuestas.encuesta_id
            )
            WHERE warning IS NULL
        ";

        $conn->executeQuery($sql);

        // Columna Procesos

        $sql = "
        UPDATE nosql_encuestas set procesos =
            (
                SELECT GROUP_CONCAT(p.nombre SEPARATOR ', ')
                    FROM survey_encuesta_proceso eproceso
                        JOIN admin_proceso p ON eproceso.proceso_id = p.id
                    WHERE eproceso.encuesta_id = nosql_encuestas.encuesta_id
            )
            WHERE procesos IS NULL
        ";
        $conn->executeQuery($sql);
    }
}
