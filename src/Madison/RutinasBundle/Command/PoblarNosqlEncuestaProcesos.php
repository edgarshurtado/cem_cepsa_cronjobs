<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption; // Para aceptar opciones por
                                                   // linea de comandos

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

/**
*
*/
class PoblarNosqlEncuestaProcesos extends ContainerAwareCommand
{

    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:poblar-nosql-encuesta-procesos') // Nombre para su ejecución
            ->setDescription('Generación tabla nosql-encuesta-procesos');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get("doctrine")->getManager();

        $conn = $em->getConnection();

        $sql = "
        INSERT INTO nosql_encuesta_procesos (id, encuesta_id, proceso_id,
            suma_valoracion, num_preguntas_valoracion,
            org_id, seg_id, neg_id, can_id, det_can_id,
            ser1_id, fecha, opi_id, g1, g1_valor, g2, g3, pais_id,
            p1, p1_pregunta_id, p2, p2_pregunta_id, p3, p3_pregunta_id,
            p4, p4_pregunta_id, opi_pregunta_id
        )

        SELECT ep_simple.id, ep_simple.encuesta_id, ep_simple.proceso_id,
            ep_simple.suma_valoracion,
            ep_simple.num_preguntas_valoracion, e.org_id,
            e.seg_id, e.neg_id, e.can_id, e.det_can_id,
            e.ser1_id, e.fecha, e.opi_id, e.g1, e.g1_valor, e.g2, e.g3, e.pais_id,
            ep_simple.p1 as p1, ep_simple.p1_id as p1_id,
            ep_simple.p2 as p2, ep_simple.p2_id as p2_id,
            ep_simple.p3 as p3, ep_simple.p3_id as p3_id,
            ep_simple.p4 as p4, ep_simple.p4_id as p4_id,
            e.opi_pregunta_id

            FROM (
                SELECT ep.id as id, er.encuesta_id as encuesta_id,
                    proceso.id as proceso_id, SUM(er.valor) as suma_valoracion,
                    count(*) num_preguntas_valoracion,
                    MAX(IF(epregunta.orden_pregunta_proceso = 1,er.valor, NULL)) as p1,
                    MAX(IF(epregunta.orden_pregunta_proceso = 1, epregunta.id, NULL)) as p1_id,
                    MAX(IF(epregunta.orden_pregunta_proceso = 2,er.valor, NULL)) as p2,
                    MAX(IF(epregunta.orden_pregunta_proceso = 2, epregunta.id, NULL)) as p2_id,
                    MAX(IF(epregunta.orden_pregunta_proceso = 3,er.valor, NULL)) as p3,
                    MAX(IF(epregunta.orden_pregunta_proceso = 3, epregunta.id, NULL)) as p3_id,
                    MAX(IF(epregunta.orden_pregunta_proceso = 4,er.valor, NULL)) as p4,
                    MAX(IF(epregunta.orden_pregunta_proceso = 4, epregunta.id, NULL)) as p4_id

                FROM survey_encuesta_respuesta er
                    JOIN survey_encuesta_pregunta epregunta
                        ON epregunta.id = er.encuesta_pregunta_id
                    JOIN admin_proceso proceso
                        ON proceso.id = epregunta.sub_bloque_id
                    JOIN survey_encuesta_proceso ep
                        ON ep.encuesta_id = er.encuesta_id
                        AND ep.proceso_id = proceso.id

                WHERE epregunta.cem_tipo_id = 4
                    AND er.valor is not null
                    AND ep.id not in (
                        SELECT id FROM nosql_encuesta_procesos
                    )
                GROUP BY encuesta_id, proceso_id
            ) ep_simple
            JOIN nosql_encuestas e ON ep_simple.encuesta_id = e.encuesta_id       ";

        $conn->executeQuery($sql);

        // Columna warning
        $sql = "
        UPDATE nosql_encuesta_procesos set warning =
            (SELECT IF(SUM(IF(er_warning.valor < 5, 1,0)) = 0, 0, 1) as warning
            FROM `survey_encuesta_respuesta` er_warning
              JOIN `survey_encuesta_pregunta` ep_warning ON er_warning.encuesta_pregunta_id = ep_warning.id
              WHERE ep_warning.cem_tipo_id = 4 && er_warning.encuesta_id = nosql_encuesta_procesos.encuesta_id
            )
            WHERE warning IS NULL
        ";

        $conn->executeQuery($sql);
    }
}
