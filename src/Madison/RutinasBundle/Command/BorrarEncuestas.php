<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

class BorrarEncuestas extends ContainerAwareCommand
{
    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:borrar-encuestas') // Nombre para su ejecución
            ->setDescription('
                Proceso para el borrado automático de todas las encuestas
            ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tiempoInicio = microtime(true);
        // Preparación de los objetos doctrine a utilizar
        $this->em = $this->getContainer()
                         ->get("doctrine")
                         ->getManager();

        $this->dql = $this->getContainer()
                          ->get("admin.dql");

        $sql = $this->getSQL();

        $db = $this->em->getConnection();

        $db->executeQuery($sql);

        // Feedback al usuario
        $tiempoEjecucion = microtime(true) - $tiempoInicio;

        $output->writeln("Se borraron las encuestas y datos asociados");
        $output->writeln("proceso realizado en: " . $tiempoEjecucion . "segundos");
    }

    private function getSQL()
    {
        return "
            BEGIN;
            DELETE FROM `survey_encuesta_respuesta` where 1;
            DELETE FROM `survey_encuesta_proceso` where 1;
            DELETE FROM `nosql_encuestas_datatable` WHERE 1;
            DELETE FROM `nosql_encuesta_procesos` where 1;
            DELETE FROM `nosql_encuestas` where 1;
            DELETE FROM `survey_encuesta` where 1;
            DELETE FROM `admin_permutaciones_vistas` where 1;
            DELETE FROM `negocio_producto` where 1;
            DELETE FROM `negocio_linea_negocio` where 1;

            DELETE FROM `admin_cliente` where 1;
            ALTER TABLE `admin_cliente` AUTO_INCREMENT = 1;
            INSERT INTO `admin_cliente` (id) VALUES (1);
            ALTER TABLE `survey_encuesta` AUTO_INCREMENT = 1;
            ALTER TABLE `survey_encuesta_respuesta` AUTO_INCREMENT = 1;
            ALTER TABLE `survey_encuesta_proceso` AUTO_INCREMENT = 1;
            ALTER TABLE `admin_permutaciones_vistas` AUTO_INCREMENT = 1;
            ALTER TABLE `negocio_producto` AUTO_INCREMENT = 1;
            ALTER TABLE `negocio_linea_negocio` AUTO_INCREMENT = 1;
            ALTER TABLE `nosql_encuestas_datatable` AUTO_INCREMENT = 1;
            ALTER TABLE `nosql_encuestas` AUTO_INCREMENT = 1;
            ALTER TABLE `nosql_encuesta_procesos` AUTO_INCREMENT = 1;

            DELETE FROM `admin_canal_det_canal` where 1;
            ALTER TABLE `admin_canal_det_canal` AUTO_INCREMENT = 1;

            DELETE FROM `admin_producto` WHERE id > 4;
            ALTER TABLE `admin_producto` AUTO_INCREMENT = 5;
            DELETE FROM `admin_negocio` WHERE id > 15;
            ALTER TABLE `admin_negocio` AUTO_INCREMENT = 16;
            COMMIT;
        ";
    }
}
