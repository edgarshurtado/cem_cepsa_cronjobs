<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption; // Para aceptar opciones por
                                                   // linea de comandos

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

/**
*
*/
class PoblarEncuestasDatatable extends ContainerAwareCommand
{

    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:poblar-nosql-encuestas-datatable') // Nombre para su ejecución
            ->setDescription('Generación datatable');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get("doctrine")->getManager();

        $conn = $em->getConnection();

        $sql = "
            INSERT INTO nosql_encuestas_datatable (encuesta_id, fecha, negocio, linea_negocio,
                canal, detalle_canal, procesos, segmento, servicio,
                experiencia, recomendacion, sentimiento, opi, warning, seg_id,
                neg_id, org_id, zon_id, pais_id, ser1_id, det_can_id
            )
            SELECT
                e.id as encuesta_id,
                e.fecha as fecha,
                neg.nombre as negocio,
                org.nombre as linea_negocio,
                can.nombre as canal,
                det_can.nombre as detalle_canal,
                 (
                    SELECT GROUP_CONCAT(p.nombre SEPARATOR ', ')
                        FROM survey_encuesta_proceso eproceso
                            JOIN admin_proceso p ON eproceso.proceso_id = p.id
                        WHERE eproceso.encuesta_id = e.id
                ) as procesos,
                seg.nombre as segmento,
                ser.nombre as servicio,
                SUM(IF(er.encuesta_pregunta_id=3,er.valor,0)) as 'experiencia',
                SUM(IF(er.encuesta_pregunta_id=2,er.valor,0)) as 'recomendacion',
                SUM(IF(er.encuesta_pregunta_id=1, er.ev, 0)) as 'sentimiento',
                opi.nombre as opi,
                (SELECT IF(SUM(IF(er_warning.valor < 5, 1,0)) = 0, 0, 1) as warning
                    FROM `survey_encuesta_respuesta` er_warning
                    JOIN `survey_encuesta_pregunta` ep_warning ON er_warning.encuesta_pregunta_id = ep_warning.id
                    WHERE ep_warning.cem_tipo_id = 4 && er_warning.encuesta_id = er.encuesta_id
                ) as 'warning',
                e.seg_id as seg,
                e.neg_id as neg,
                e.org_id as org,
                e.zon_id as zon,
                e.pais_id as pais,
                e.ser1_id as ser1,
                e.det_can as det_can
                FROM survey_encuesta_respuesta er
                    JOIN survey_encuesta e ON e.id = er.encuesta_id
                    JOIN survey_encuesta_pregunta_opi opi ON opi.id = e.opi_pregunta_id
                    join admin_negocio neg ON e.neg_id = neg.id
                    JOIN admin_canal_encuestacion can ON e.can_id = can.id
                    JOIN admin_linea_negocio org ON e.org_id = org.id
                    JOIN admin_determinacion_canal det_can ON e.det_can = det_can.id
                    JOIN admin_segmento seg ON e.seg_id = seg.id
                    JOIN admin_servicio ser ON e.ser1_id = ser.id
                WHERE e.id NOT IN (
                    SELECT encuesta_id FROM nosql_encuestas_datatable
                )
                GROUP BY e.id        ";

        $conn->executeQuery($sql);
    }
}
