<?php
namespace Madison\RutinasBundle\Command;

// Clase de Command en la cual se puede obtener el objeto container usado para
// obtener las rutas de los assets
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

// Formatea la salida por consola
use Symfony\Component\Console\Style\SymfonyStyle;

// Dependencias necesarias para cuando haces un comando en Symfony
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption; // Para aceptar opciones por
                                                   // linea de comandos

// Clases para usar Doctrine
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

// Entidad doctrine en la que persistir las nuevas permutaciones encontradas
// en la bd
use AdminBundle\Entity\PermutacionesVistas;

// Librería de Fechas
use AdminBundle\Library\FechasUtils;

class PoblarPermutacionesVistas extends ContainerAwareCommand
{
    /**
     * Configuración del comando
     */
    protected function configure()
    {
        $this
            ->setName('madison:poblar-permutaciones-vistas') // Nombre para su ejecución
            ->setDescription('Genera en la base de datos las permutaciones para
                las opciones de visualización en Cuadro de Mando')
            ->addOption(
                "rango",
                "t",
                InputOption::VALUE_NONE,
                "Leer toda la base de datos?"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Preparación de los objetos doctrine a utilizar
        $this->em = $this->getContainer()
                         ->get("doctrine")
                         ->getManager();

        $this->dql = $this->getContainer()
                          ->get("admin.dql");

        // Preparación de las variables para dar feedback al usuario
        $tiempoInicio = new \DateTime();
        $tiempoInicio = $tiempoInicio->getTimestamp();
        $filasInsertadas = 0;
        $filasProcesadas = 0;

        $sql = "
            SELECT DISTINCT p.id indicador, ep.proceso_id proceso, e.neg_id negocio , e.can_id canal, sb.id subbloque
            FROM survey_encuesta_respuesta r
                    JOIN survey_encuesta_pregunta p ON p.id = r.encuesta_pregunta_id
                    JOIN survey_encuesta_sub_bloque sb ON sb.id = p.sub_bloque_id
                    JOIN survey_encuesta e ON e.id = r.encuesta_id
                    JOIN survey_encuesta_proceso ep ON ep.encuesta_id = e.id
                    WHERE p.cem_tipo_id IS NOT NULL
                        AND (ep.proceso_id = sb.id OR sb.id IN (1))
        ";

        $procesarTodo = $input->getOption("rango");
        if(!$procesarTodo){
            $temporalidadIdMes = 2;
            $sql = $this->dql->sqlFiltrarPorTemporalidad(
                "e", $temporalidadIdMes, new \DateTime(), $sql
            );
        }

        foreach($this->dql->generadorResultadosSQL($sql) as $row){ // Iteración sobre los resultados
                                                                       // que devuelve el generador

            if($this->filaEnBD($row)) continue; // En caso de que la combinación
                                                // ya se encuentre en la BD.

            $this->em->persist($this->creaObjeto($row)); // Adición de la nueva
                                                         // fila a la cola de
                                                         // persistencia.

            if($filasInsertadas % 100 == 0) // Persistir por paquetes
            {
                $this->em->flush();
                $this->em->clear();
            }

            $filasInsertadas++;
        }

        // Persistir remanente
        $this->em->flush();
        $this->em->clear();


        // Feedback al usuario
        $tiempoEjecucion = new \DateTime();
        $tiempoEjecucion = $tiempoEjecucion->getTimestamp() - $tiempoInicio;

        $output->writeln("Se han insertado " . $filasInsertadas . " filas.");
        $output->writeln("proceso realizado en: " . $tiempoEjecucion . " ms.");

        if($filasInsertadas == 0)
        {
            $output->writeln("");
            $output->writeln("Puede que hayas olvidado ejecutar"
                            . " madison:poblar-encuestas-procesos antes?");
        }

        if(!$input->getOption("rango")){
            $output->writeln("");
            $output->writeln("Puedes probar `madison:poblar-permutaciones-vistas -t` para "
                            ."recorrer toda la base de datos");

        }
    }


    /**
     * Función que genera el sql a ejecutar en la lógica principal
     */
    private function getSQL($procesarTodo)
    {
        $sql = "
            SELECT DISTINCT p.id indicador, ep.proceso_id proceso, e.neg_id negocio , e.can_id canal, sb.id subbloque
            FROM survey_encuesta_respuesta r
                   JOIN survey_encuesta_pregunta p ON p.id = r.encuesta_pregunta_id
                   JOIN survey_encuesta_sub_bloque sb ON sb.id = p.sub_bloque_id
                   JOIN survey_encuesta e ON e.id = r.encuesta_id
                   JOIN survey_encuesta_proceso ep ON ep.encuesta_id = e.id
                   WHERE p.cem_tipo_id IS NOT NULL
                   AND (ep.proceso_id = sb.id OR sb.id IN (1,13,14))
        ";

        // Opción por defecto para la query. Sólo cogerá las encuestas a día de hoy
        if(!$procesarTodo){
            // Con el código a continuación se comprueba desde el mes pasado
            // hasta el actual (Para evitar conflictos en la carga de un día 31
            // en el día 1 o 2 del mes siguiente)
            $temporalidadIdMes = 2;
            $periodos = 2;
            $aliasTablaFiltrado = "e";

            $rangoFechas = FechasUtils::getRangoTemporalidadId(
                $temporalidadIdMes, new \DateTime(), $periodos
            );

            $sql = $dql->sqlAddRangoFechas(
                $aliasTablaFiltrado, $rangoFechas["fecha_inicio"],
                $rangoFechas["fecha_fin"], $sql
            );
        }

        return $sql;
    }

    private function filaEnBD($fila)
    {
        $objEnBD = $this->em
                        ->getRepository("AdminBundle:PermutacionesVistas")
                        ->findOneBy(array(
                            "indicador" => $fila["indicador"],
                            "proceso" => $fila["proceso"],
                            "negocio" => $fila["negocio"],
                            "canal" => $fila["canal"],
                            "subBloque" => $fila["subbloque"]
                        ));

        if($objEnBD) return true;

        return false;
    }


    private function creaObjeto($fila)
    {
        $obj = new PermutacionesVistas();

        $obj->setIndicador($this->getObjById(
            "SurveyBundle:EncuestaPregunta" , $fila["indicador"])
        );

        $obj->setProceso($this->getObjById(
            "SurveyBundle:EncuestaSubBloque" , $fila["proceso"])
        );

        $obj->setNegocio($this->getObjById(
            "AdminBundle:Negocio" , $fila["negocio"])
        );

        $obj->setCanal($this->getObjById(
            "AdminBundle:CanalEncuestacion" , $fila["canal"])
        );

        $obj->setSubBloque($this->getObjById(
            "SurveyBundle:EncuestaSubBloque" , $fila["subbloque"])
        );

        return $obj;
    }

    private function getObjById($repositoryName, $idValue){
        return $this->em->getRepository($repositoryName)->findOneById($idValue);
    }
}
