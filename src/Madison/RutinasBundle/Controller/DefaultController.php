<?php

namespace Madison\RutinasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

// Imports para poder usar comandos desde un controlador

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response("Seleccione una rutina para ejecutar");
    }

    /**
     * @Route("/{comando}")
     * @Route("/{comando}/{opcion}")
     */
    public function generarPermutaciones($comando, $opcion = "" )
    {
        $rutaBase = $this->get('kernel')->getRootDir() . "/../";
        chdir($rutaBase); // Para ejecutar los comandos desde la ruta base del
                          // proyecto

        if(!empty($opcion) && $opcion != "t")
            return new Response("Opción no válida");

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        // --  Creación de la configuración del comando a ejecutar
        $infoInput = array(
            "command" => $this->getComando($comando)
        );

        if(!empty($opcion) && $opcion == "t")
            $infoInput["-t"] = true;

        $input = new ArrayInput($infoInput);

        $output = new BufferedOutput();
        $application->run($input, $output);

        $content = $output->fetch();

        return new Response($content);
    }

    private function getComando($comandoPeticion)
    {
        $cmd = "";

        switch ($comandoPeticion) {
            case 'poblar-permutaciones-vistas':
                $cmd = "madison:poblar-permutaciones-vistas";
                break;
            case 'poblar-encuesta-procesos':
                $cmd = "madison:poblar-encuesta-procesos";
                break;
            case 'borrar-encuestas':
                $cmd = "madison:borrar-encuestas";
                break;
            case 'poblar-nosql-encuestas':
                $cmd = "madison:poblar-nosql-encuestas";
                break;
            case 'poblar-nosql-encuesta-procesos':
                $cmd = "madison:poblar-nosql-encuesta-procesos";
                break;
            case 'poblar-nosql-encuestas-datatable':
                $cmd = "madison:poblar-nosql-encuestas-datatable";
            default:
                # code...
                break;
        }

        return $cmd;
    }
}
