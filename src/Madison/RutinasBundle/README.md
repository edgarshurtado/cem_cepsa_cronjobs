# Madison/RutinasBundle

Este bundle ha sido creado con el objetivo de dotar a la consola de Symfony con
una serie de rutinas para facilitar el desarrollo de sus cms.

## Instalación

* Instalar dependencias:

```bash
composer require assetic-bundle
composer require leafo/scssphp
composer require symfony/console
```

* Registrar bundle en el `AppKernel.php`
* Escribir rutas de archivos scss para procesar

Ahora ya se puede ejecutar el comando desde consola con:

```bash
php app\console comprime:css
```

## Ejecución automática desde app_dev.php

Con esta configuración, se ejecutará el proceso cada vez que se acceda al
proyecto por entorno de desarrollo

* Importar dependencias:

```php
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
```

```php
$application = new Application($kernel);
$application->setAutoExit(false);
$input = new ArrayInput(array("command" => "comprime:css"));

$output = new NullOutput();

$application->run($input, $output);
```