<?php
namespace ImportBundle\Service;

use ImportBundle\Service\ErroresService;

/**
 * Esta clase ha sido desarrollada de forma de que sea lo suficientemente
 * general como para no tener que modificarla. Para añadir lógica de tests
 * hazlo en FilaValidacion.php
 */
class CamposValidacionLogica
{
    function __construct(
        ErroresService $erroresService
    ){

        $this->erroresService = $erroresService;

    }

    public function setArrayConfiguracion($arrayConfiguracion)
    {
        $this->arrayConfiguracion = $arrayConfiguracion;

        // Definición de los atributos que corresponden a los campos del
        // archivo excel
        foreach ($this->getAtributos() as $atr) {
            $this->$atr = NULL;
        }

        return $this;
    }

    public function setFila($fila)
    {

        // Comprobación de que se haya asignado primero el array de configuración
        if(empty($this->arrayConfiguracion))
            throw new \Exception("No hay seteado un array de Configuración", 1);

        // Asignación de los valores a los atributos previamente definidos.
        foreach ($fila as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    public function __get($campo)
    {
        return $this->$campo;
    }

    /**
     * Función que ejecuta toda la lógica necesaria para saber si la línea
     * valida correctamente.
     *
     * Devuelve un array con los errores encontrados.
     *
     * @return array
     */
    public function isOk()
    {
        // Comprobación de que se haya asignado primero el array de configuración
        if(empty($this->arrayConfiguracion))
            throw new \Exception("No hay seteado un array de Configuración", 1);

        $atributos = $this->getAtributos();
        $erroresEncontrados = array();

        foreach ($atributos as $atr) {
            $resultadoTest = $this->testCampo($atr);
            if($resultadoTest != NULL){
                $erroresEncontrados[$atr] = $resultadoTest;
            }
        }
        return $erroresEncontrados;
    }

    /**
     * Devuelve un array con el conjunto total de atributos que han sido
     * configurados en el array de configuración
     */
    protected function getAtributos()
    {
        $ac = $this->arrayConfiguracion;
        $arrayAtrs = [];

        foreach ($ac as $ob) {
            foreach ($ob["atributos"] as $key => $value) {
                $arrayAtrs[] = $key;
            }
        }

        return $arrayAtrs;
    }


    /**
     * Función que ejecuta todos los tests especificados para el campo que se le
     * pasa.
     *
     * Devuelve un array con todos los errores producidos para ese campo en la
     * ejecución de los tests.
     */
    protected function testCampo($campo)
    {
        $arrayTests = $this->getAtrsConfig("tests");

        $erroresCampo = [];

        foreach ($arrayTests[$campo] as $validacion) {
            // Generar el nombre del test
            $test = "test" . ucfirst($validacion);
            // Obtener los errores asociados al test con el campo
            $errores = $this->$test($campo);
            // Si $errores == null es porque el campo es válido
            if($errores != NULL){
                $erroresCampo[] = $errores;
            }
        }

        return $erroresCampo;
    }

    /**
     * Devuelve un array con todos los atributos presentes en el array de
     * configuración con el campo solicitado.
     * Actualmente se gasta para obtener los tests o qué tipo de dato contiene
     * la celda
     */
    protected function getAtrsConfig($dato)
    {
        $ac = $this->arrayConfiguracion;

        $arrayModos = [];

        foreach ($ac as $obj) {
            foreach ($obj["atributos"] as $atributo => $valores) {
                $arrayModos[$atributo] = $valores[$dato];
            }
        }

        return $arrayModos;
    }


    // FUNCIONES ÚTILES necesarias para la lógica interna

    protected function existe($valorCampo)
    {
        return $valorCampo !== NULL
               && $valorCampo !== "";
    }
}