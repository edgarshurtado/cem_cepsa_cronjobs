<?php

namespace ImportBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Acl\Exception\Exception;
use ImportBundle\Service\FilaValidacion;
use ImportBundle\Service\PersistirFila;
use Doctrine\ORM\EntityManager;
use ImportBundle\Service\ErroresService;

class ValidacionesService
{
    private $container;
    private $em;
    private $hojaExcel;

    public function __construct(Container $container, EntityManager $em, ErroresService $es)
    {

        $this->container = $container;
        $this->em = $em;
        $this->erroresService = $es;
        set_time_limit(0); // Para evitar la interrupción de inserciones
                           // grandes en la base de datos por rebasar el tiempo
                           // de conexión límite.

        ini_set("memory_limit", "150M");
    }

    /**
     * Une la configuración estática con la configuración dinámica que coincida
     * con la cabecera del array de filas resultado de haber leído el archivo
     * que ha de persistirse.
     *
     * @param  [type] $configuracionEstatica Configuración definida en el service.yml.
     * @param  [type] $configuracionDinamica Configuración opcional en service.yml.
     * @param  [type] $cabecera              Permite tomar en consideración las
     *                                       preguntas que no están presentes en
     *                                       la configuración estática y que coinciden
     *                                       con los sufijos definidos en la
     *                                       configuración dinámica.

     * @return [type]                        El array con la configuración para
     *                                       iniciar la persistencia de datos.
     */
    private function creaArrayConfiguracion(
        $configuracionEstatica,
        $configuracionDinamica,
        $cabecera
    ){
        $arrayDinamico = [];
        // Uno todas los títulos de las celdas para poder trabajar más fácilmente
        // con la regEx
        $stringCabecera = implode(",", $cabecera);

        foreach ($configuracionDinamica["listadoSufijos"] as $sufijo) {
            $regEx = "/" . $sufijo . "[0-9]+/";
            // Búsqueda de todas las coincidencias para el sufijo sobre el
            // que se está iterando. Las coincidencias son almacenadas en
            // $matches[0]. Mirar documentación preg_match_all() para más
            // información
            preg_match_all($regEx, $stringCabecera, $matches);

            foreach ($matches[0] as $campo) {

                // Por si la pregunta ha sido definida manualmente en el array
                // configuración
                if(isset($configuracionEstatica[$campo])){
                    continue;
                }

                $arrayDinamico[$campo] = $this->configuracionCampo(
                    $configuracionDinamica["plantillaConfiguracion"],
                    $campo,
                    $sufijo
                );
            }
        }

        // Unión de la parte estática con la generada dinámicamente
        $arrayConfiguracion = array_merge($configuracionEstatica,
                                          $arrayDinamico);

        return $arrayConfiguracion;
    }

    private function configuracionCampo($plantillaConfiguracion, $campo, $sufijo)
    {
        $configCampo = $plantillaConfiguracion;

        $configCampo["sufijo"] = strtolower($campo);
        $configCampo["comprobacion"] = strtolower($sufijo);

        /*
            Cambio de la clave "nombreAtributo" por el nombre real del
            atributo que se está haciendo en ese momento
        */
        // Set de la nueva clave
        $configCampo["atributos"][$campo] =
            $configCampo["atributos"]["nombreAtributo"];
        // Eliminar la antigua clave
        unset($configCampo["atributos"]["nombreAtributo"]);

        return $configCampo;
    }


    /**
     * Función principal con la lógica de procesado de todas las filas del
     * archivo exel especificado.
     *
     * Devuelve un array con los errores encontrados
     */
    public function procesarArchivo($rutaArchivo)
    {
        $filas = $this->getArrayFilasFichero($rutaArchivo);

        // Comprobación de que se ha podido leer la cabecera correctamente

        if(!isset($filas[0])){
            // Se devuelve como array puesto que se espera que esta función
            // devuelva un array de errores.
            return array($this->erroresService->generarArrayError("ERROR010"));
        }

        $cabecera = array_keys($filas[0]);

        // Lógica de procesado del archivo normal una vez tenemos una cabecera
        // válida.
        $configuracionDinamica = $this->container->hasParameter("import.camposDinamicos") ?
                                $this->container->getParameter("import.camposDinamicos")
                                : ["listadoSufijos" => []];

        $arrayConfiguracion = $this->creaArrayConfiguracion(
            $this->container->getParameter("import.arrayConfiguracion"),
            $configuracionDinamica,
            $cabecera
        );

        $errores = [];

        for ($i=0; $i < sizeof($filas); $i++) {
            $errores[$i + 1] = // $i + 1 para que el índice coincida con el
                                // número de la fila
                $this->procesaFila($filas[$i], $arrayConfiguracion);
        }

        return $errores;
    }


    /**
     * Crea un ObjetoValidacion que comprueba la validez de la fila. Si no hay
     * errores se procede a la persistencia de la fila.
     */
    public function procesaFila($arrayCeldas, $arrayConfiguracion)
    {

        $cv = $this->container->get("import.camposvalidacion")
                   ->setArrayConfiguracion($arrayConfiguracion)
                   ->setFila($arrayCeldas);

        $erroresFila = $cv->isOk();

        if(count($erroresFila) == 0){
            $pfs = $this->container->get("import.persistir")
                        ->inicializarService($cv, $arrayConfiguracion);

            $erroresPersistencia = $pfs->persistir();
            return $erroresPersistencia;
        }

        return $erroresFila;
    }

    /**
     * Función que elige el service que hay que lanzar para la obtención de los
     * datos del excel
     *
     * @return Array Array de arrays asociativos que representan las filas del
     * fichero donde las claves son los nombres de las columnas y su valor el
     * dato contenido en la celda.
     */
    private function getArrayFilasFichero($rutaArchivo)
    {
        $arrayResultado = [];

        switch ($this->getArchivoTipo($rutaArchivo)) {
            case 'csv':
                $delimitador = ";";
                $arrayResultado = $this->container
                                        ->get("import.csv")
                                        ->leerArchivo($rutaArchivo, $delimitador);
                break;
            case 'excel':
                $arrayResultado = $this->container
                                        ->get("import.excel")
                                        ->leerArchivo($rutaArchivo);
            default:
                # code...
                break;
        }

        return $arrayResultado;
    }

    private function getArchivoTipo($rutaArchivo)
    {
        $rutaArchivo = explode("/", $rutaArchivo);
        $nombreFichero = $rutaArchivo[sizeof($rutaArchivo) - 1];
        $posicionPunto = strpos($nombreFichero, ".");

        $tipo = substr($nombreFichero, $posicionPunto + 1);

        $resultado = "";
        switch ($tipo) {
            case 'csv':
                $resultado = "csv";
                break;
            case 'xls':
                $resultado = "excel";
                break;
            default:
                break;
        }

        return $resultado;
    }

}