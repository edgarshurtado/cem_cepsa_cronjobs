<?php

namespace ImportBundle\Service;

/**
*
*/
class ParsearExcelService
{

    function __construct()
    {
    }

    public function leerExcel($rutaExcel, $hoja = 0)
    {
        return $this->setArchivo($rutaExcel, $hoja)
                    ->obtenerFilas();
    }

    /**
     * Configuración inicial del archivo a procesar. Por defecto coge la hoja
     * primera del archivo excel.
     */
    public function setArchivo($rutaArchivo, $hoja = 0)
    {

        /**
         * Creación del objeto de phpExcel que nos da acceso a
         * funcionalidades de lectura y escritura
         */
        $objPHPExcel = \PHPExcel_IOFactory::load($rutaArchivo);

        // Configuración y obtención de la hoja de excel sobre la que
        // trabajar
        $objPHPExcel->setActiveSheetIndex($hoja);
        $this->hojaExcel = $objPHPExcel->getActiveSheet();

        return $this;
    }

    /**
     * Función principal con la lógica de procesado de todas las filas del archivo
     * exel especificado.
     *
     * Devuelve un array con los errores encontrados
     */
    public function obtenerFilas()
    {
        /**
         * Comprobación que se ha asignado una hoja de excel activa.
         */
        if(!isset($this->hojaExcel)){
            throw new Exception("No se ha cargado un archivo
                exel. Usa el método 'setArchivo' para ello", 1);
        }else {
            $hojaExcel = $this->hojaExcel;
        }


        /**
         * Procesado del archivo
         */
        // Variables de iteración
        $nFilas = $hojaExcel->getHighestRow();
        $nColumnas = $hojaExcel->getHighestColumn();

        $errores = [];  // Acumulador de errores que será lo que devuelve
                        // la función

        // Sacamos todos los nombres de campos del archivo
        $campos = $this->getNombreCampos($nColumnas);

        $documento = [];

        for($fila = 2; $fila <= $nFilas; $fila++){ // Obviamos la primera fila
                                                  // que corresponde a los nombres
                                                  // de los campos

            $arrayFila = []; // Acumulador de celdas para formar filas

            $col = 'A';
            foreach ($campos as $campo) {
                $celda = $hojaExcel->getCell($col . $fila);
                $arrayFila[$campo] = $celda->getValue();
                $col++;
            }

            $documento[] = $arrayFila;
        }

        return $documento;
    }

    /**
     * Devuelve un array con los valores de la primera fila de la hoja
     * excel
     */
    private function getNombreCampos($maxCol)
    {

        $maxCol++;
        $columns = $this->letrasRange($maxCol);



        $nombresCampos = [];
        for ($i = 0; $i < count($columns); $i++) {
            $valorCelda = $this->hojaExcel
                                    ->getCell($columns[$i] . 1)
                                    ->getValue();

            if($valorCelda !== NULL && $valorCelda !== ''){
                $nombresCampos[] = $valorCelda;
            }else{
                continue;
            }
        }

        return $nombresCampos;
    }

    /**
     * Devuelve un array con todas las letras incluídas en el rango
     * especificado
     */
    private function letrasRange($letraFin, $letraInicio="A"){

        // Comprobación previa de que hay un rango positivo
        $ordLetraFin = ord($letraFin);
        $ordLetraInicio = ord($letraInicio);
        if($ordLetraFin - $ordLetraInicio <= 0) return [];

        // Generación del array de resultado
        $array = [];
        $letraActual = $letraInicio;

        while($letraActual !== $letraFin){
            $array[] = $letraActual;
            $letraActual++;
        }

        return $array;
    }
}