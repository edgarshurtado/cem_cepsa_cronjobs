<?php
namespace ImportBundle\Service;
/**
*
*/
class ParsearCsvService
{

    function __construct()
    {
    }

    public function leerArchivo($archivo, $delimitador, $longitudLinea = 0)
    {
        $punteroArchivo = fopen($archivo, "r");

        $filas = [];
        $campos = fgetcsv($punteroArchivo, $longitudLinea, $delimitador);

        // Eliminación del caracter BOM
        $campos[0] = $this->remove_utf8_bom($campos[0]);

        // Montar array de filas donde cada fila será a su vez un array en el
        // que los datos estarán organizados por los campos. Esto se puede hacer
        // porque los datos se leen de forma secuencial y el array campos y el
        // array fila van a estar siempre sincronizados (mismo índice representa
        // la misma posición)
        while(($fila = fgetcsv($punteroArchivo, $longitudLinea, $delimitador)) !== FALSE){
            $datosFila = [];
            for ($i=0; $i <  sizeof($campos); $i++) {
                $campo = $campos[$i];
                $datosFila[$campo] = $fila[$i];
            }

            $filas[] = $datosFila;
        }

        fclose($punteroArchivo); // Cierre del archivo por seguridad


        return $filas; // TODO: Cambiar por return
    }

    // Función que elimina el caracter BOM de un texto
    private function remove_utf8_bom($text)
    {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}