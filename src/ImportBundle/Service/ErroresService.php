<?php

namespace ImportBundle\Service;

/**
*
*/
class ErroresService
{

    /**
     * Devuelve un array de error.
     */
    public function generarArrayError($cod, $info=[])
    {
        $arrayError = $info;

        $arrayError = array_merge($arrayError, array(
            "codErr" => $cod,
            "descripcion" => $this->descripcionError($cod)
        ));

        return $arrayError;
    }

    /**
     * Función de ayuda para generarArrayError. devuelve la descripción del
     * error según el código de error proporcionado
     */
    private function descripcionError($cod){
        $descripcion='';
        switch ($cod) {
            case 'ERROR001':
                $descripcion = 'Falta dato obligatorio';
                break;
            case 'ERROR002':
                $descripcion ='Dato desconocido';
                break;
            case 'ERROR003':
                $descripcion ='Formato incorrecto';
                break;
            case 'ERROR004':
                $descripcion ='Fecha fuera de rango';
                break;
            case 'ERROR005':
                $descripcion = 'Encuesta previamente creada';
                break;
            case 'ERROR006':
                $descripcion = 'No hay una encuesta válida en esta fila';
                break;
            case 'ERROR007':
                $descripcion = "No es una fecha válida";
                break;
            case 'ERROR008':
                $descripcion = "El campo ha de ser una palabra o frase";
                break;
            case 'ERROR009':
                $descripcion = "El campo ha de ser un valor numérico";
                break;
            case 'ERROR010':
                $descripcion = "No se han podido leer las filas del archivo";
                break;
            default:
                $descripcion ='Error desconocido';
                break;
        }
        return $descripcion;
    }

}