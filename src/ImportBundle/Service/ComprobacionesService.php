<?php
namespace ImportBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ComprobacionesService extends ComprobacionesLogica
{
    // Propiedades `protected` para poder ser usadas por **ComprobacionesLogica**
    protected $doctrine;
    protected $em;
    protected $repositorio;
    protected $container;

    //---------------------- COMPROBACIONES ESPECÍFICAS ------------------------
    /**
     * Aquí van todas las comprobaciones que tengan que hacerse sobre un objeto
     * superior. Estas funciones han de usar el nombre definido en `comprobación`:
     * `comprobacionNombreEntidad`
     *
     * Una vez realizada toda la lógica que se programe, esta función ha de
     * devolver una instancia del mismo tipo de objeto que se configuró con
     * `setComprobacion`.
     *
     * Estas comprobaciones han de ser `protected` puesto que serán llamadas
     * desde **ComprobacionesLogica**
     */

    protected function comprobacionCliente()
    {


        $telf = $this->objeto->getTelf();
        $nif = $this->objeto->getNif();
        $mail = $this->objeto->getMail();

        $clienteEnBD = $this->doctrine
                        ->getRepository($this->repositorio)
                        ->findOneByNif($this->objeto->getNif());

        if(isset($clienteEnBD)) {
            $this->objeto = $clienteEnBD;

        }elseif(empty($telf) && empty($nif) && empty($mail)){
            // Set a un cliente standar por si no hay información significativa
            // del cliente. (Evita inserciones de clientes sin información)
            $this->objeto =
                $this->doctrine
                     ->getRepository($this->repositorio)
                     ->findOneById(1);
        }

        return $this->objeto;
    }

    protected function comprobacionNegocio()
    {
        $negocio = $this->objeto->getNombre();

        $negocioStr = mb_strtolower($negocio, "UTF-8");

        switch ($negocioStr) {
            case 'e-commerce': case 'ecommerce':
                $negocioStr = "ecommerce";
                break;
            case "vvdd" : case "vv.dd" : case "ventas directas":
                $negocioStr = 'ventas directas';
                break;
            case 'asfaltos': case 'proas':
                $negocioStr = 'lubricantes';
                break;
            case 'aviación': case 'aviacion':
                $negocioStr = 'aviacion';
                break;
            default:
                # code...
                break;
        }


        $negocioEnBD = $this->doctrine
                        ->getRepository($this->repositorio)
                        ->findOneByNombre($negocioStr);

        if(!is_null($negocioEnBD)) {
            $this->objeto = $negocioEnBD;
        }else { // Sólo se permiten los negocios registrados en la BD previamente
            $this->objeto =
                $this->erroresService->generarArrayError(
                    "ERROR002", ["negocio" => $negocioStr]
                );
        }

        return $this->objeto;
    }

    protected function comprobacionLineaNegocio()
    {

        $this->objeto = $this->campoOrSinEspecificar(
            $this->objeto,
            $this->repositorio
        );

        return $this->objeto;
    }

    // Alerta CG. Probablemente esto se podría generalizar para todas
    // las comprobaciones de entidades con 2 relaciones
    protected function comprobacionNegocioLineaNegocio()
    {
        // Parto de la base de que los objetos negocio y producto en el array
        // de persistencia, ya han sido comprobados para crear uno nuevo o
        // coger de la BD.
        if(isset($this->ObjetosFila["AdminBundle:Negocio"])){
            $negocio = $this->ObjetosFila["AdminBundle:Negocio"];
        }else {
            $negocio = null;
        }

        if(isset($this->ObjetosFila["AdminBundle:LineaNegocio"])){
            $lineaNegocio =
                $this->ObjetosFila["AdminBundle:LineaNegocio"];
        }else {
            $lineaNegocio = null;
        }

        // Si no se ha seteado el negocio o linea de negocio previamente, no
        // hay que persistir nada en NegocioLineaNegocio
        if(empty($negocio) || empty($lineaNegocio)){
            return NULL;
        }

        $negocioLineaNegocioRepo = $this->doctrine
                                        ->getRepository($this->repositorio);

        $nln = $negocioLineaNegocioRepo->findOneBy(array(
            "negocio"  => $negocio,
            "lineaNegocio" => $lineaNegocio
        ));

        if(!is_null($nln)){
            $this->objeto = $nln;
        }else{
            // Si no se encuentra el objeto en la BD, se prepara el
            // nuevo objeto vacio para ser persistido
            $this->objeto->setNegocio($negocio);
            $this->objeto->setLineaNegocio($lineaNegocio);
        }

        return $this->objeto;
    }


    protected function comprobacionCanal()
    {
        $nombreCanal = $this->objeto->getNombre();
        $nombreCanal = strtolower($nombreCanal);

        switch ($nombreCanal) {
            case 'email': case 'e_mail': case 'e-mail':
                $nombreCanal = "mail";
                break;
            case 'flayer':
                $nombreCanal = 'flyer';
                break;
            case 'telf':
                $nombreCanal = 'telefono';
                break;
            case 'tiquet':
                $nombreCanal = "ticket";
                break;
        }

        $canal = $this->em->getRepository($this->repositorio)
                      ->findOneByNombre($nombreCanal);

        if(is_null($canal)){
            $this->objeto = $this->erroresService->generarArrayError(
                "ERROR002", ["canal" => $nombreCanal]
            );
        }else{
            $this->objeto = $canal;
        }

        return $this->objeto;
    }

    protected function comprobacionPais()
    {
        $pais = $this->objeto->getNombre();

        $paisStr = mb_strtolower($pais, "UTF-8");
        $paisStr = ucfirst($paisStr);

        $paisEnBD = $this->doctrine
                        ->getRepository($this->repositorio)
                        ->findOneByNombre($paisStr);

        if(!is_null($paisEnBD)) {
            $this->objeto = $paisEnBD;
        }else {
            $this->objeto =
                $this->erroresService
                     ->generarArrayError(
                        "ERROR002", ["pais" => $paisStr]
                     );
        }

        return $this->objeto;
    }


    protected function comprobacionDeterminacionCanal()
    {

        $this->objeto = $this->campoOrSinEspecificar(
            $this->objeto,
            $this->repositorio
        );

        return $this->objeto;
    }

    protected function comprobacionCanalDeterminacionCanal()
    {
        if(isset($this->ObjetosFila["AdminBundle:CanalEncuestacion"])){
            $canal = $this->ObjetosFila["AdminBundle:CanalEncuestacion"];
        }else {
            $canal = null;
        }
        if(isset($this->ObjetosFila["AdminBundle:DeterminacionCanal"])){
            $determinacionCanal =
                $this->ObjetosFila["AdminBundle:DeterminacionCanal"];
        }else {
            $determinacionCanal = null;
        }

        if(empty($canal) || empty($determinacionCanal)){
            return NULL;
        }

        $canalDetCanalRepo = $this->doctrine
                                  ->getRepository($this->repositorio);

        $cdc = $canalDetCanalRepo->findOneBy(array(
            "canal"  => $canal,
            "determinacionCanal" => $determinacionCanal,
        ));

        if(!is_null($cdc)){
            $this->objeto = $cdc;
        }else{
            // Si no se encuentra el objeto en la BD, se prepara el
            // nuevo objeto vacio para ser persistido
            $this->objeto->setCanal($canal);
            $this->objeto->setDeterminacionCanal($determinacionCanal);
        }

        return $this->objeto;
    }

    protected function comprobacionPreguntaOpi()
    {
        $valorCampo = $this->objeto->getNombre();

        $preguntaOpiEnBD = $this->doctrine->getRepository($this->repositorio)
                                ->findOneByNombre($this->objeto->getNombre());

        if(!is_null($preguntaOpiEnBD)){
            $this->objeto = $preguntaOpiEnBD;
        }

        return $this->objeto;
    }

    protected function comprobacionEncuesta()
    {

        $encuestaEnBD = $this->doctrine
                            ->getRepository($this->repositorio)
                            ->findOneByOpiId($this->objeto->getOpiId());

        if(is_object($encuestaEnBD)) {
            $this->objeto =
                $this->erroresService->generarArrayError(
                    "ERROR005", ["encuesta" => $encuestaEnBD->getOpiId()]
                );
        }else {
            //Comprobaciones del objeto anidado Segmento
            $this->objeto->setSeg(
                $this->campoOrSinEspecificar(
                    $this->objeto->getSeg(),
                    "AdminBundle:Segmento"
                )
            );
            // Seteo de cliente previamente comprobado
            $this->objeto->setCliente(
                $this->ObjetosFila["AdminBundle:Cliente"]
            );

            // Seteo de Negocio previamente comprobado
            if(isset($this->ObjetosFila["AdminBundle:Negocio"])){
                $this->objeto->setNeg(
                    $this->ObjetosFila["AdminBundle:Negocio"]
                );
            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["Negocio" => "No válido"]
                );
            }

            // Seteo de Linea de negocio previamente comprobada
            if(isset($this->ObjetosFila["AdminBundle:LineaNegocio"])){
                $this->objeto->setOrg(
                    $this->ObjetosFila["AdminBundle:LineaNegocio"]
                );
            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["LineaNegocio" => "No válido"]
                );
            }

            if(isset($this->ObjetosFila["AdminBundle:CanalEncuestacion"])){
                $this->objeto->setCan(
                    $this->ObjetosFila["AdminBundle:CanalEncuestacion"]
                );
            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["Canal" => "No válido"]
                );
            }

            // Seteo de La Determinación de canal previamente comprobada
            if(isset($this->ObjetosFila["AdminBundle:DeterminacionCanal"])){
                $this->objeto->setDetCan(
                    $this->ObjetosFila["AdminBundle:DeterminacionCanal"]
                );
            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["Determinación de Canal" => "No válido"]
                );
            }

            // Set del Pais previamente creado
            if(isset($this->ObjetosFila["AdminBundle:Pais"])){
                $this->objeto->setPais(
                    $this->ObjetosFila["AdminBundle:Pais"]
                );
            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["País" => "No válido"]
                );
            }

            // Set de la pregunta opi (OPI en el excel)
            if(isset($this->ObjetosFila["SurveyBundle:EncuestaPreguntaOpi"])){
                $this->objeto->setOpiPregunta(
                    $this->ObjetosFila["SurveyBundle:EncuestaPreguntaOpi"]
                );

            }else{
                return $this->erroresService->generarArrayError(
                    "ERROR002", ["OPI" => "No válido"]
                );
            }

            // Set de los servicios
            $ser1 = $this->objeto->getSer1();
            $ser1 = $this->campoOrSinEspecificar(
                $ser1, "AdminBundle:Servicio"
            );
            $this->objeto->setSer1($ser1);

            $ser2 = $this->objeto->getSer2();
            $ser2 = $this->campoOrSinEspecificar(
                $ser2, "AdminBundle:Servicio"
            );
            $this->objeto->setSer2($ser2);

            $ser3 = $this->objeto->getSer3();
            $ser3 = $this->campoOrSinEspecificar(
                $ser3, "AdminBundle:Servicio"
            );
            $this->objeto->setSer3($ser3);

            $ser4 = $this->objeto->getSer4();
            $ser4 = $this->campoOrSinEspecificar(
                $ser4, "AdminBundle:Servicio"
            );
            $this->objeto->setSer4($ser4);
        }

        return $this->objeto;
    }


    /**
     * Obtención de un objeto de la base de datos o el del valor por defecto
     * "sin especificar".
     *
     * Valido para entidades doctrine con el campo Nombre
     */
    protected function campoOrSinEspecificar($objetoDoctrine, $repositoryString)
    {
        $campoDefinitivo = $objetoDoctrine;

        $valorCampo = $objetoDoctrine->getNombre();

        if(empty($valorCampo)){
            $valorCampo = "sin especificar";
        }

        $campoEnBD = $this->doctrine
                          ->getRepository($repositoryString)
                          ->findOneByNombre($valorCampo);

        if(!is_null($campoEnBD)) {
            $campoDefinitivo = $campoEnBD;
        }

        return $campoDefinitivo;
    }

    // TODO: Permitir inserciones de varios productos
    protected function comprobacionProducto()
    {
        $nombreProducto = $this->objeto->getNombre();

        if(empty($nombreProducto)){
            $nombreProducto = "sin especificar";
        }

        $productoEnBD = $this->doctrine
                             ->getRepository($this->repositorio)
                             ->findOneByNombre($nombreProducto);

        if(isset($productoEnBD)) {
            $this->objeto = $productoEnBD;
        }

        return $this->objeto;
    }
    protected function comprobacionNegocioProducto()
    {
        // Parto de la base de que los objetos negocio y producto en el array
        // de persistencia, ya han sido comprobados para crear uno nuevo o
        // coger de la BD.
        if(isset($this->ObjetosFila["SurveyBundle:Encuesta"])){
            $negocio = $this->ObjetosFila["SurveyBundle:Encuesta"]->getNeg();
        }else {
            $negocio = null;
        }

        $producto = $this->ObjetosFila["AdminBundle:Producto"];

        if(empty($negocio) || empty($producto)){
            return NULL;
        }

        $negocioProductoRepo = $this->doctrine
                                    ->getRepository($this->repositorio);

        $np = $negocioProductoRepo->findOneBy(array(
            "negocio"  => $negocio,
            "producto" => $producto
        ));

        if(!empty($np)){
            $this->objeto = $np;
        }else{
            $this->objeto->setNegocio($negocio);
            $this->objeto->setProducto($producto);
        }

        return $this->objeto;
    }


    protected function comprobacionG()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }

    protected function comprobacionG1()
    {
        $equivalenciasAdjetivos = array(
            "Confiad@" => 7,
            "Cuidad@" => 7,
            "Decepcionad@" => 2,
            "Enfadad@" => 0,
            "Gratamente sorprendid@" => 10,
            "Ignorad@" => 4
        );

        $this->objeto = $this->comprobacionG();

        if(is_null($this->objeto)) return null; // Caso de que la encuesta sea
                                                // inválida

        if(gettype($this->objeto) != "array"){
            $adj = $this->objeto->getValor();
            if(array_key_exists($adj, $equivalenciasAdjetivos)){
                $this->objeto->setEv(
                    $equivalenciasAdjetivos[$this->objeto->getValor()]
                );
            }else {
                $this->objeto =
                    $this->erroresService
                         ->generarArrayError(
                            "ERROR002",
                            ["pregunta" => $this->nombreObjeto, "valor" => $adj]
                        );
            }
        }

        return $this->objeto;
    }

    protected function comprobacionG2()
    {
        return $this->comprobacionG();
    }
    protected function comprobacionG3()
    {
        return $this->comprobacionG();
    }

    protected function comprobacionTip()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionSer()
    {

        $this->objeto = $this->campoOrSinEspecificar(
            $this->objeto,
            $this->repositorio
        );

        return $this->objeto;
    }
    protected function comprobacionA()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionOf()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionPc()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionE()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionP()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionF()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionGc()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionAt()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionB()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionS()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionPs()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionN()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionO()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }
    protected function comprobacionPa()
    {
        return $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );
    }

    protected function comprobacionPa2()
    {
        // Se pasa el objeto por la comprobación general de pregunta para
        // realizarle las tareas que necesitan todos los objetos de una entidad
        // de respuesta
        $objetoPrimerProcesado = $this->comprobacionGenericaPregunta(
            $this->objeto, $this->nombreObjeto
        );

        if(is_null($objetoPrimerProcesado))
            return NULL;

        // Si $objetoPrimerProcesado es un array es porque la comprobación
        // genérica de pregunta ha fallado y devuelve el array de error
        if(gettype($objetoPrimerProcesado) == "array")
            return $objetoPrimerProcesado;

        // Creación de los objetos según la cantidad de datos que hubieran en
        // la celda separados por "|"
        $datosEnPa2 = $objetoPrimerProcesado->getValor();
        $datosEnPa2 = explode("|", $datosEnPa2);

        // Realizamos el proceso de creación de diversos objetos EncuestaRespuesta
        // sólo si se han detectado varios datos contenidos en la misma celda.
        if(sizeof($datosEnPa2) > 1){
            $arrayEncuestaRespuestas = $this->generarMultiplesEncuestaRespuesta(
                $datosEnPa2,
                "SurveyBundle\Entity\EncuestaRespuesta",
                $objetoPrimerProcesado
            );

            // Creación y inserción de los objetos en el array de persistencia
            $this->preparacionPersistenciaMultiple(
                $arrayEncuestaRespuestas, "SurveyBundle:EncuestaRespuesta_pa2"
            );


            // Al hacerse la persistencia directamente modificando
            // $this->ObjetosFila, hay que devolver null para que el objeto
            // original no sea persistido
            return NULL;
        }else { // Devolución del objeto simple (Caso de que no hayan varios datos)

            return $objetoPrimerProcesado;
        }
    }

    /**
     * Comprobaciones básicas y genéricas de todas las preguntas.
     * La referencia se refiere al id de la pregunta en la tabla EncuestaPregunta
     *
     * Devuelve NULL si el objeto no se ha de persistir.
     * Un array de error si se ha producido algún error que haya que notificar
     * Un objeto listo para persistir si no se ha dado ninguno de los casos
     * anteriores.
     */
    private function comprobacionGenericaPregunta($obj, $nombreObjeto)
    {

        $valor = $obj->getValor();
        if($valor == null || empty($nombreObjeto)) return NULL;

        // Obtención de la Pregunta a la que pertenece la respuesta
        $nombre = $nombreObjeto;
        $repoPreguntas =
            $this->em->getRepository("SurveyBundle:EncuestaPregunta");

        $ePregunta = $repoPreguntas->findOneByNombre($nombre);

        if(is_null($ePregunta)){
            $ePregunta = new \SurveyBundle\Entity\EncuestaPregunta();
            $ePregunta->setNombre($nombre);
        }

        // Set de la EncuestaPregunta al objeto EncuestaRespuesta
        $obj->setEncuestaPregunta($ePregunta);

        // Error si no hay una encuesta válida
        if(!isset($this->ObjetosFila["SurveyBundle:Encuesta"])){
            return null;
        }else{
            // Seteamos la encuesta para la respuesta
            $obj->setEncuesta($this->ObjetosFila["SurveyBundle:Encuesta"]);
        }

        return $obj;
    }

    /**
     * A partir de un array de datos, crea un array de objetos de la entidad
     * especificada. Los datos son seteados en el atributo `valor` de cada
     * ojeto.
     *
     * @param  [type] $arrayDatos          [description]
     * @param  [type] $entidadPersistencia [description]
     * @return [type]                      [description]
     */
    public function generarMultiplesEncuestaRespuesta(
        $arrayDatos, $entidadPersistencia, $objPlantilla
    ){
       // Comprobación de la validez de los parámetros de la función
       if(gettype($entidadPersistencia) !== "string"
        ){
            throw new \Exception("Error en los tipos de datos de respuesta múltiple", 1);
       }

       $encuesta = $objPlantilla->getEncuesta();
       $pregunta = $objPlantilla->getEncuestaPregunta();

       $arrayRespuestas = [];

       foreach ($arrayDatos as $dato) {
            $objeto = new $entidadPersistencia();
            $objeto->setValor($dato);
            $objeto->setEncuesta($encuesta);
            $objeto->setEncuestaPregunta($pregunta);

            $arrayRespuestas[] = $objeto;
       }

       return $arrayRespuestas;
    }
}