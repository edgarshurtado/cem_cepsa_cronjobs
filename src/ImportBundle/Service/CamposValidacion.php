<?php
namespace ImportBundle\Service;
use ImportBundle\Service\FilaValidacionLogica;

class CamposValidacion extends CamposValidacionLogica
{
    // ------------------------- TESTS -------------------------------------
    /**
     * Las funciones han de nombrarse en camelCase como: testNombreTest. La
     * palabra test ha de estar siempre. El nombre para el test ha de coincidir
     * con alguno de los especificados en el array de configuracion del bundle
     *
     * Las funciones de test recibirán el nombre del campo como parámetro.
     *
     * Los test han de devolver un mensaje de error cuando no pasen una
     * verificación. Cuando el test es exitoso ha de devolver `NULL`.
     *
     * Para la generación de un array de resultados utilizar la función
     * '$this->generarArrayError' pasándole el nombre del campo que se está
     * testeando y el código del error.
     *
     * Escribir sólo un test por mensaje de error. La lógica de ImportBundle
     * ya se encarga de ejecutar todos los test unitarios descritos y generar
     * un array con todos los errores que se hayan producido
     */

    protected function testExiste($campo)
    {
        if(!$this->existe($this->__get($campo))){
            return $this->erroresService
                ->generarArrayError("ERROR001", ["valor" => $this->__get($campo)]);
        }
        return NULL;
    }

    protected function testFecha($campo)
    {

        if(!$this->existe($campo))
            return $this->erroresService
                ->generarArrayError("ERROR001", ["valor" => $this->__get($campo)]);

        try {
            $fecha =  \DateTime::createFromFormat(
                "d/m/Y H:i:s",
                $this->$campo
            );

            if(!($fecha instanceof \DateTime)){
                return $this->erroresService
                    ->generarArrayError("ERROR004", ["valor" => $this->__get($campo)]);
            }

            $this->$campo = $fecha;

            $fechaTs = $fecha->getTimestamp();
            $hoyTs = (new \DateTime())->getTimestamp();

            if(!$fecha->getTimestamp() > 0
                && $hoyTs - $fechaTs < 0
            ){
                return $this->erroresService
                    ->generarArrayError("ERROR004", ["valor" => $this->__get($campo)]);
            }

            return NULL;

        } catch (Exception $e) {
            return $this->erroresService
                ->generarArrayError("ERROR001", ["valor" => $this->__get($campo)]);
        }
    }

    protected function testString($campo)
    {
        $valorCampo = $this->__get($campo);

        if($valorCampo == NULL) return NULL;

        if(ctype_digit($valorCampo))
            return $this->erroresService
                ->generarArrayError("ERROR008", ["valor" => $valorCampo]);

        // Parseo del valor para eliminar el ZERO WIDTH SPACE y los espacios
        // en blanco de los laterales.
        $valorCampo = preg_replace( '/[\x{200B}-\x{200D}]/u', '', $valorCampo);
        $valorCampo = trim($valorCampo, " ​");
        $this->$campo = $valorCampo;

        return NULL;
    }

    protected function testNumero($campo)
    {

        $valorCampo = $this->__get($campo);

        if($valorCampo == NULL) return NULL;

        if(!ctype_digit($valorCampo)){
            return $this->erroresService
                ->generarArrayError("ERROR009", ["valor" => $valorCampo]);
        }

        return NULL;
    }

}