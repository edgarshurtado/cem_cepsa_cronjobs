<?php

namespace ImportBundle\Service;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
*
*/
class ComprobacionesLogica
{

    function __construct(Container $c)
    {
        $this->container = $c;
        $this->doctrine = $c->get('doctrine');
        $this->em = $c->get('doctrine')->getManager();

        // Service para la generación de mensajes de error
        $this->erroresService = $c->get("import.errores");

    }

    /**
     * Configura todos los campos que van a ser necesarios para la lógica de las
     * comprobaciones.
     */
    public function setComprobacion(
        $objeto, $bundle ,$entidad,
        $nombreComprobacion, $nombreObjeto
    ){
        unset($this->objeto);
        $this->objeto = $objeto;
        $this->repositorio = $bundle . ":" . $entidad;
        $this->comprobacion = $nombreComprobacion;
        $this->nombreObjeto = $nombreObjeto;

        return $this;
    }


    /**
     * Función que asigna el array de objetos que se están persistiendo para
     * la fila actual. Permite al service tener acceso a los objetos que ya
     * se prepararon para persistir por si alguna comprobación necesita tener
     * acceso a estos.
     */
    public function setArrayObjetosFila(&$arrayObjetosFila)
    {
        $this->ObjetosFila = &$arrayObjetosFila;

        return $this;
    }

    public function ejecutarComprobacion()
    {
        // Creación del nombre de la funcion a llamar asegurando que la primera
        // letra de la entidad esté en mayúscula
        $funcionComprobacion = "comprobacion" . ucfirst($this->comprobacion);
        $objetoComprobado = $this->$funcionComprobacion();

        return $objetoComprobado;
    }


    /**
     * Inserta múltiples objetos en el array de persistencias `$this->ObjetosFila`
     * @param  [type] $objetos   [description]
     * @param  [type] $namespace [description]
     * @return [type]            [description]
     */
    public function preparacionPersistenciaMultiple($objetos, $namespace)
    {
        $objetosPersistidos = 0;

        foreach ($objetos as $objeto) {
            // Adición de un sufijo para que no haya sobreescritura de los
            // objetos en el array
            $namespaceNuevo = $namespace . "_$objetosPersistidos";

            // Inserción del objeto en el array que posteriormente se utiliza
            // para la persistencia de la fila
            $this->ObjetosFila[$namespaceNuevo] = $objeto;

            $objetosPersistidos++;
        }

        return true;
    }
}