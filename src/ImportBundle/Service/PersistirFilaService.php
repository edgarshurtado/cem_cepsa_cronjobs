<?php
namespace ImportBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use ImportBundle\Service\CamposValidacion;

class PersistirFilaService
{
    private $fila;
    private $container;
    private $objetosAPersistir = []; // Array de objetos que serán persistidos por
                                     // doctrine
    private $comprobacionesService;
    private $arrayConfigFila; // Array con la configuración de la estructura de
                              // las filas y la información necesaria para
                              // su persistencia.

    function __construct(Container $container)
    {
        $this->container = $container;


        $this->comprobacionesService = $container->get('import.comprobaciones');
    }

    /**
     * Recibe un objeto de la clase CamposValidacion y el array de configuración
     * para la carga.
     */
    public function inicializarService(CamposValidacion $cv, $arrayConfiguracion)
    {
        $this->fila = $cv;

        $this->arrayConfigFila = $arrayConfiguracion;

        return $this;
    }

    public function crearObjetos()
    {
        $fila = $this->fila;
        $this->objetosAPersistir = [];

        foreach ($this->arrayConfigFila as $nombreObjeto =>$objetoDatos) {
            // Creación de un nuevo objeto a persistir
            $clase = $objetoDatos["bundle"] . "\Entity\\" . $objetoDatos["entidad"];
            $objeto = new $clase();

            // Iteración sobre todos los atributos que ha de tener el objeto
            // Atención: Los atributos no son guardados en el array de
            // persistencia
            foreach ($objetoDatos["atributos"] as $atrNombre => $atrDatos) {

                // Obtener el nombre de la columna en la base de datos
                $nombreCampoEnBD = $this->getNombreCampoEnEntidad($atrDatos,
                    $atrNombre);
                $funcSet = $this->nombreFuncionDoctrine($nombreCampoEnBD);

                $valorParaSet = "";

                $atrNamespace = $this->getNamespace($atrDatos);


                if(isset($this->objetosAPersistir[$atrNamespace])){
                    // Caso en el cual el atributo es una referencia a un objeto
                    // anteriormente creado. Para discriminar esto se busca si
                    // el namespace está creado ya en el array de persistencia.
                    // En caso afirmativo, este objeto se utilizará como valor
                    // del atributo
                    $valorParaSet = $this->objetosAPersistir[$atrNamespace];
                }else{
                    // En caso de no tratarse de un objeto previamente creado,
                    // se procede a la búsqueda o creación del valor que le
                    // corresponda al atributo.
                    $valorCelda = $fila->__get($atrNombre);
                    $valorParaSet = $this->getOrCreate(
                        $objetoDatos, $atrDatos, $valorCelda
                    );
                }

                // Set del valor que se tendrá que persistir
                $objeto->$funcSet($valorParaSet);

            }
            // Se añade el objeto nuevo al array general con su bundle + entidad
            // como clave para poder hacer la búsqueda en los campos que sean
            // dependientes. Esto es que cualquier atributo que sea
            // una instancia de un objeto anteriormente creado utilizará este
            // buscándolo por este "namespace" creado.
            $objNamespace = $this->getNamespace($objetoDatos);

            $this->objetosAPersistir[$objNamespace] = $objeto;
        }
    }

    private function realizarComprobaciones()
    {

        $erroresComprobacion = [];
        foreach ($this->arrayConfigFila as $nombreObjeto => $objetoDatos) {
            $objNamespace = $this->getNamespace($objetoDatos);

            $objetoActualPersistir = null;
            $objetoActualPersistir = clone $this->objetosAPersistir[$objNamespace];

            $objetoDefinitivo = $this->comprobacionesService
                                    ->setArrayObjetosFila($this->objetosAPersistir)
                                    ->setComprobacion(
                                        $objetoActualPersistir,
                                        $objetoDatos["bundle"],
                                        $objetoDatos["entidad"],
                                        $objetoDatos["comprobacion"],
                                        $nombreObjeto
                                    )->ejecutarComprobacion();

            // Para capturar los errores de comprobaciones. comprobacionesService
            // devuelve un array si ha habido algún error o un objeto preparado
            // para persistir si no ha habido ninguno.
            if(is_array($objetoDefinitivo)){
                $erroresComprobacion[] = $objetoDefinitivo;
                // Dado a que ha hay un error, el objeto no se ha de persistir
                // y se destruye.
                $objetoDefinitivo = NULL;
                unset($this->objetosAPersistir[$objNamespace]);
            }else{
                // Reasignamos por si comprobacionesService ha realizado algún
                // cambio en el objeto, o por si ha dado un error y no se ha de
                // persistir el objeto.
                $this->objetosAPersistir[$objNamespace] = $objetoDefinitivo;
            }

       }

        return $erroresComprobacion;
    }

    /**
     * Obtiene el valor de un atributo para entidad doctrine, ya sea del tipo
     * de valor simple (directamente de la celda) o una instancia de doctrine.
     * Si el atributo es de la misma entidad que el padre, devuelve el valor,
     * si no, crea un objeto nuevo.
     */
    private function getOrCreate($datosObjetoPadre, $datosAtributo, $valor)
    {
        $bundle = $datosAtributo["bundle"];
        $entidad = $datosAtributo["entidad"];
        $resultado = NULL;
        $identificador = "Nombre";

        $identificador = isset($datosAtributo["identificador"]) ?
            $datosAtributo["identificador"] : "Nombre";


        if($entidad == $datosObjetoPadre["entidad"]){
            // Se trata de un valor simple
            $resultado = $valor;
        } else {
            // Es una instancia de un objeto
            $resultado = $this->creaNuevoObjeto(
                $bundle, $entidad, $valor, $identificador
            );
        }


        return $resultado;
    }

    /**
     * Obtiene la instancia de un objeto. Primero hace una búsqueda en base de
     * datos usando el valor proporcionado en el campo designado por el
     * identificador. Si no se encuentra, se crea un objeto nuevo tras ser
     * añadido al array de persistencia
     */
    private function creaNuevoObjeto($bundle, $repositorio, $valor
        , $identificador)
    {
        $rep = $this->container->get('doctrine')
            ->getRepository($bundle . ":" . $repositorio);

        $funGet = "findOneBy" . $identificador;
        $obj = $rep->$funGet($valor);

        if(!$obj){
            $clase = $bundle . "\Entity\\" . $repositorio;
            $obj = new $clase();
            $funSet = "set" . $identificador;
            $obj->$funSet($valor);
        }

        return $obj;
    }

    public function nombreFuncionDoctrine($nombre, $metodo = "set")
    {
        $nombre = strtolower($nombre);
        $nombre = ucwords($nombre, "_");
        $nombre = str_replace("_", "", $nombre);

        return $metodo . $nombre;
    }

    private function getNombreCampoEnEntidad($atrDatos, $atrNombre)
    {
        $nombreCampoEnBD = "";
        if(isset($atrDatos["nombreCampo"])){
            $nombreCampoEnBD = $atrDatos["nombreCampo"];
        }else {
            $nombreCampoEnBD = $atrNombre;
        }

        return $nombreCampoEnBD;
    }

    /**
     * Recorre el array de persistencia y lo persiste. Mejor comentario ever!!
     */
    public function persistir()
    {
        // var_dump(array_keys($this->objetosAPersistir));
        // var_dump($this->objetosAPersistir["SurveyBundle:Encuesta"]);
        $this->crearObjetos();
        $erroresComprobacion = $this->realizarComprobaciones();

        $em = $this->container->get('doctrine')->getManager();


        foreach ($this->objetosAPersistir as $namespace => $obj) {
            if(is_object($obj)){
                $em->persist($obj);
            }
        }
        $em->flush();
        $em->clear();

        return $erroresComprobacion;
    }

    private function getNamespace($datos)
    {

        $namespace = $datos["bundle"] . ":" . $datos["entidad"];

        // Este diferenciador existe para poder tener varias instancias de
        // una misma entidad en el array. Esto hace también que estos objetos
        // no puedan ser referenciados por otro atributo ya que se rompe la
        // estructura de los namespace.
        if(isset($datos["sufijo"]))
            $namespace .= "_" . $datos["sufijo"];

        return $namespace;
    }
}