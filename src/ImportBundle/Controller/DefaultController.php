<?php

namespace ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Acl\Exception\Exception;

// Librería para uso de SFTP con certificado RSA
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;


class DefaultController extends Controller
{

    /**
     * @Route("/autocarga")
     *
     * Controlador que automáticamente coje el último archivo presente en la
     * carpeta con los archivos de carga y redirige la petición al proceso de
     * carga estándar con este archivo como parámetro.
     */
    public function autoload(Request $request)
    {
        $logger = $this->get("log.slack");

        $dirArchivos = $this->getUploadDir();
        $strAyer = date("Y-m-d", strtotime( '-1 days' ));
        $nombreFichero = "opinions-".$strAyer.".csv";
        $rutaCertificado = $dirArchivos . "mdmk.ppk";

        if(!file_exists($dirArchivos.$nombreFichero)){
          $isArchivoDescargado = $this->descargarUltimoArchivoSFTP(
            "193.148.246.186", $dirArchivos,
            "/EXCHANGE/deopinator2/",
            $nombreFichero,
            $rutaCertificado,
            "madison"
          );

          if(!$isArchivoDescargado){
            $msg = "El archivo $nombreFichero no se pudo descargar";
            $logger->error($msg);
            throw new \Exception($msg, 1);
          }
        }

        $respuestaParaElNavegador = $this->procesoCarga($dirArchivos.$nombreFichero);
        $this->procesosTrasCarga();

        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContent($respuestaParaElNavegador);

        return $response;
    }

    /**
     * @Route("/cargar-todo")
     */
    public function cargarTodo(Request $request)
    {
        $logger = $this->get("log.slack");

        $hostDestino = $this->getParameter("database_host");
        $dbDestino = $this->getParameter("database_name");
        $nombreProyecto = $this->getParameter("project");

        $logger->alert("
          Se ha iniciado un proceso de carga completa en $nombreProyecto. Esto puede tardar...
          BD destino : $hostDestino/$dbDestino
          :vaporeon:
        ");

        $uploadDir = $this->getUploadDir();
        $lsDirectorio = scandir($this->getUploadDir());

        $filasPersistidas = 0;
        foreach ($lsDirectorio as $archivoCarga) {

            if(strpos($archivoCarga, ".csv") > 0
                || strpos($archivoCarga, ".xls") > 0){

                $filasPersistidas += $this->procesoCarga($uploadDir.$archivoCarga);

            }
        }

        $usarTodaBD = true;
        $this->procesosTrasCarga($usarTodaBD);


        $respuestaParaElNavegador = $this->procesoSatisfactorio($filasPersistidas);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContent($respuestaParaElNavegador);

        return $response;
    }

    /**
     * @Route("/descargar-todo")
     *
     * Función que descargará todos los archivos de un directorio remoto por sftp
     */
    public function descargarTodo(Request $request)
    {
      $dirDestino = $this->getUploadDir();
      $rutaCertificado = $dirDestino . "mdmk.ppk";

      $sftp = new SFTP("193.148.246.186");

      $Key = new RSA();

      $Key->loadKey(file_get_contents($rutaCertificado));

      if(!$sftp->login("madison", $Key)){
        throw new \Exception("Error login to SFTP", 1);
      }

      $sftp->chdir("/EXCHANGE/deopinator2/");
      $listadoArchivos = $sftp->nlist();

      $nArchivos = 0;
      foreach($listadoArchivos as $archivo){
        if(!file_exists($dirDestino.$archivo)){
          $sftp->get($archivo, $dirDestino.$archivo);
          $nArchivos++;
        }
      }

      $response = new Response();
      $response->headers->set('Content-Type', 'text/plain');
      $response->setContent("Proceso finalizado con $nArchivos descargados");
      return $response;
    }



    /**
     * @Route("/", name="carga_un_fichero")
     */
    public function indexAction(Request $request)
    {
        set_time_limit(0);

        $logger = $this->get("log.slack");
        $hostDestino = $this->getParameter("database_host");
        $dbDestino = $this->getParameter("database_name");
        $nombreProyecto = $this->getParameter("project");

        $logger->alert("
          Se ha iniciado un proceso de carga en $nombreProyecto
          BD destino : $hostDestino/$dbDestino
          :vaporeon:
        ");

        $nombreArchivo = $request->query->get('file');

        $rutaArchivo = $this->getUploadDir() . $nombreArchivo;

        $respuestaParaElNavegador = "";

        try{
            $filasPersistidas = $this->procesoCarga($rutaArchivo);

            $usarTodaBD = false;
            $this->procesosTrasCarga($usarTodaBD);

            $respuestaParaElNavegador =
                $this->procesoSatisfactorio($filasPersistidas);

        }catch(\Exception $e){
            if($e->getCode() == 1){
                $respuestaParaElNavegador = $this->archivoNoExite($rutaArchivo);
            }else{
                $respuestaParaElNavegador = $e->getMessage();
            }
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');
        $response->setContent($respuestaParaElNavegador);

        return $response;
    }

    private function procesoCarga($rutaArchivo)
    {

        if(!file_exists($rutaArchivo)){
            throw new \Exception("El archivo $rutaArchivo no existe", 1);
        }

        $validacionesService = $this->get("import.validaciones");

        $respuestaParaElNavegador = "";

        $erroresCarga = $validacionesService
                           ->procesarArchivo($rutaArchivo);

        $filasPersistidas = $this->logErrores($erroresCarga, $rutaArchivo);


        return $filasPersistidas;
    }

    private function logErrores($errores = [], $nombreArchivo)
    {
        set_time_limit(0);

        // Descarta de la ruta del archivo todo lo que no sea nombre fichero
        $nombreArchivo = explode("/", $nombreArchivo);
        $nombreArchivo = end($nombreArchivo);

        $rutaArchivoLog = $this->get('kernel')->getRootDir(). "/../app/logs/";

        // Cambia formato de csv a log
        $nombreFicheroLog =  str_replace("csv", "log", $nombreArchivo);

        // Proceso escritura
        $archivoLog = fopen($rutaArchivoLog.$nombreFicheroLog,"w");
        $fechaCarga = new \DateTime();
        $fechaCarga = $fechaCarga->format("d/m/Y H:i");

        $totalFilas = sizeof($errores);

        fwrite($archivoLog, "#Carga realizada: $fechaCarga. Archivo: $nombreArchivo.");
        fwrite($archivoLog, " Procesadas $totalFilas filas\n" );

        $fila = 2;
        $filasConError = 0;

        foreach ($errores as $erroresFila) {
            $nErroresFila = sizeof($erroresFila);
            if($nErroresFila > 0){
                fwrite($archivoLog, "##FILA $fila\n");
                fwrite($archivoLog, json_encode($erroresFila));
                fwrite($archivoLog, "\n");
                $filasConError++;
            }
            $fila++;
        }

        $filasPersistidas = $totalFilas - $filasConError;

        fwrite($archivoLog, "* Se han persistido correctamente $filasPersistidas filas\n");
        fwrite($archivoLog, "* Filas con errores: $filasConError\n");

        fwrite($archivoLog, "----------------FINAL CARGA---------\n\n");

        fclose($archivoLog);

        $logger = $this->get("log.slack");
        $logger->info("
          Procesado archivo $nombreArchivo
          Se han persistido correctamente $filasPersistidas filas
          Filas con errores: $filasConError

          :deal_with_it:
        ");

        return $filasPersistidas;
    }

    private function procesosTrasCarga($usarTodaBD = false)
    {
        $this->ejecutaComandoEnTodaBd(
          'madison:poblar-encuesta-procesos', $usarTodaBD);
        $this->ejecutaComandoEnTodaBd(
          'madison:poblar-permutaciones-vistas', $usarTodaBD);

        // No necesitan el parámetro $usarTodaBD porque la sql que se ejecuta
        // ya se encarga de insertar sólo los datos nuevos.
        $this->ejecutarComandoSinArgumentos("madison:poblar-nosql-encuestas-datatable");
        $this->ejecutarComandoSinArgumentos("madison:poblar-nosql-encuestas");
        $this->ejecutarComandoSinArgumentos("madison:poblar-nosql-encuesta-procesos");

        return true;
    }

    private function ejecutaComandoEnTodaBd($comando, $usarTodaBD)
    {
        $kernel = $this->get('kernel');

        $application = new Application($kernel);
        $application->setAutoExit(false);

        $arrayInputOptions = array(
           'command' => $comando
        );

        if($usarTodaBD){
            $arrayInputOptions["-t"] = true;
        }

        $input = new ArrayInput($arrayInputOptions);

        $output = new NullOutput(); // No necesitamos el output
        $application->run($input, $output);

        return true;

    }

    private function ejecutarComandoSinArgumentos($comando)
    {
        $kernel = $this->get('kernel');

        $application = new Application($kernel);
        $application->setAutoExit(false);

        $arrayInputOptions = array(
           'command' => $comando
        );

        $input = new ArrayInput($arrayInputOptions);

        $output = new NullOutput(); // No necesitamos el output
        $application->run($input, $output);

        return true;
    }

    private function procesoSatisfactorio($filasPersistidas)
    {

        return "Han sido persistidas correctamente $filasPersistidas filas

            Estamos condenados
                           \  .-.
                             /_ _\
                             |o^o|
                             \ _ /
                            .-'-'-.
                          /`)  .  (`\            BLEEP BOOP BLOOP
                         / /|.-'-.|\ \         /
                         \ \| (_) |/ /  .-''-.
                          \_\'-.-'/_/  /[] _ _\
                          /_/ \_/ \_\ _|_o_LII|_
                            |'._.'|  / | ==== | \
                            |  |  |  |_| ==== |_|
                             \_|_/    ||' ||  ||
                             |-|-|    || o  o ||
                             |_|_|    ||'----'||
                            /_/ \_\  /__|    |__\
        ";

    }

    private function archivoNoExite($rutaArchivo)
    {
        return    "
                El archivo $rutaArchivo no existe
                        __.,,------.._
                     ,''   _      _   '`.
                    /.__, ._  -=- _'`    Y
                   (.____.-.`      ''`   j
                    VvvvvvV`.Y,.    _.,-'       ,     ,     ,
                        Y    ||,   ''\         ,/    ,/    ./
                        |   ,'  ,     `-..,'_,'/___,'/   ,'/   ,
                   ..  ,;,,',-''\,'  ,  .     '     ' ''' '--,/    .. ..
                 ,'. `.`---'     `, /  , Y -=-    ,'   ,   ,. .`-..||_|| ..
                ff\\`. `._        /f ,'j j , ,' ,   , f ,  \=\ Y   || ||`||_..
                l` \` `.`.'`-..,-' j  /./ /, , / , / /l \   \=\l   || `' || ||...
                 `  `   `-._ `-.,-/ ,' /`'/-/-/-/-'''''`.`.  `'.\--`'--..`'_`' || ,
                            '`-_,',  ,'  f    ,   /      `._    ``._     ,  `-.`'//         ,
                          ,-''' _.,-'    l_,-'_,,'          '`-._ . '`. /|     `.'\ ,       |
                        ,',.,-''          \=) ,`-.         ,    `-'._`.V |       \ // .. . /j
                        |f\\               `._ )-.'`.     /|         `.| |        `.`-||-\\/
                        l` \`                 '`._   '`--' j          j' j          `-`---'
                         `  `                     '`_,-','/       ,-''  /
                                                 ,'',__,-'       /,, ,-'
                                                 Vvv'            VVv'

                                     -=> Philip Kaulfuss <=-
        ";

    }

// ------- Controllers de testeo ----------

    /**
     * @Route("/csv")
     */
    public function parsearCsv(Request $request)
    {
        $parseadorCsv = $this->get("import.csv");

        $nombreArchivo = $this->getParameter("directorio_archivos_carga") . "prueba.csv";

        var_dump($parseadorCsv->leerCsv($nombreArchivo, ";"));
    }

    /**
     * @Route("/excel")
     */
    public function parsearExcel(Request $request)
    {
        $parseadorCsv = $this->get("import.excel");

        $nombreArchivo = $this->getUploadDir() . "prueba.xls";

        if(!$this->isArchivoValido($nombreArchivo)){
            throw new Exception(
                "El archivo " . $nombreArchivo . " no existe o es inválido", 1
            );
        }

        var_dump($parseadorCsv->leerExcel($nombreArchivo));
    }

    /**
     * Devuelve la ruta al directorio que contiene los archivos de carga de
     * datos. Para cambiar este directorio por defecto, recomiendo ir al
     * archivo parameters.yml y crear el parámetro 'directorio_archivos_carga'
     * con la ruta del directorio que se necesite como valor.
     *
     * Si no está este parámetro, la ruta por defecto es web/uploads
     */
    private function getUploadDir(){

        $nombreParametro = "directorio_archivos_carga";
        if($this->container->hasParameter($nombreParametro)){
            return $this->container->getParameter($nombreParametro);
        }
        return $this->container->get('kernel')->getRootDir()
                . "/../web/uploads/";
    }

    /**
     * Comprobación de que se haya pasado un nombre de
     * archivo y que existe. Si sólo se pone la comprobación
     * de que el archivo exista, en el caso de que no haya
     * nombre de archivo, como el directorio base sí que existe
     * pasaría la comprobación dando un error más adelante.
     */
    private function isArchivoValido($rutaArchivo){
        return isset($rutaArchivo) && file_exists($rutaArchivo);

    }

    private function descargarUltimoArchivoSFTP(
      $dominio, $dirDestino, $dirRemoto, $nombreArchivo, $rutaCertificado, $usuario
    ){
      $sftp = new SFTP($dominio);

      $Key = new RSA();

      $Key->loadKey(file_get_contents($rutaCertificado));

      if(!$sftp->login($usuario, $Key)){
        throw new \Exception("Error login to SFTP", 1);
      }
      $rutaCompletaArchivoRemoto = $dirRemoto . $nombreArchivo;

      return $sftp->get(
        $rutaCompletaArchivoRemoto, $dirDestino.$nombreArchivo
      );
    }
}
