# ImportBundle MadisonMK

Este bundle ha sido desarrollado por Edgar S. Hurtado para MadisonMK
(Telecyl S.L.) © All rights reserved.

El código ha sido profundamente comentado para facilitar su uso y entendimiento.

ImportBundle recibe a través de su service principal (ValidacionesService) un
archivo el cual carga, valida cada fila, registra en la bd todas aquellas sin
errores y finalmente devuelve un array con todos los posibles errores que hayan
surgido en el proceso de validación.


Acciones de configuración previas necesarias a su uso:
* Instalar PHPExcel [repositorio](https://github.com/liuggio/ExcelBundle)
    + `$composer require liuggio/excelbundle`
* adición del Bundle en AppKernel

```php
function registerBundles()
    {
        $bundles = array(

            // ...
            // Otros registros de bundle del sistema
            // ...
            new ImportBundle\ImportBundle(),
        );
    }
```

* Adición de la importación del archivo services.yml del bundle en congif.yml
    + `{ resource: "@ImportBundle/Resources/config/services.yml" }`
* Configuración en services.yml del bundle:
    + Configuración de la ruta de carga donde se alojarán los ficheros
        + en el parametro `directorio_archivos_carga`
    + Configuración del array de configuración con los datos relevantes del
    proyecto.
* Configuración de las comprobaciones por cada una de las clases principales
descritas en la configuración del bundle en comprobacionesService.php
    - A modo de comprobación básica se puede usar:

```php
public function comprobacionNombreEntidad()
{
    return $this->objeto;
}
```


> ¡¡IMPORTANTE!! Las entidades doctrine de persistencia han de contar con la
opción de cascade={"persist"} para el correcto funcionamiento del bundle

Para su uso, simplemente realizar desde algún controller o cualquier objeto PHP
con acceso a `@service_container`:

```php
// Desde un controlador
$errores = $this->get("import.validaciones")
            ->setArchivo($nombreArchivo)
            ->procesarArchivio();

// Desde cualquier clase php con acceso a @service_container
$errores = $container->get("import.validaciones")
            ->setArchivo($nombreArchivo)
            ->procesarArchivio();
```

## Preparación del array de configuración.

Leer la documentación en services.yml.

## Limitaciones

* Actualmente, `ImportBundle` no admite las relaciones M:M de forma directa.
Cuando se tengan relaciones de este tipo habrá que crearse una entidad nueva
que represente la tabla generada en la base de datos por la relación M:M

* Actualmente, una vez configurado, el bundle sólo funciona con un único archivo
de carga. Para cambiar de archivo habría que sustituir la configuración.

## Posibles mejoras

* Permitir que se puedan definir varios array de configuración para tener la
opción de utilizar el bundle con distintos ficheros de carga